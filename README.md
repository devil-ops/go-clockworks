# Go Clockworks

[![pipeline status](https://gitlab.oit.duke.edu/devil-ops/go-clockworks/badges/main/pipeline.svg)](https://gitlab.oit.duke.edu/devil-ops/cartographer-sdk/-/commits/main)
[![coverage report](https://gitlab.oit.duke.edu/devil-ops/go-clockworks/badges/main/coverage.svg)](https://gitlab.oit.duke.edu/devil-ops/cartographer-sdk/-/commits/main)

Re-doing the original clockworks-sdk, but without the auto generated pieces from the OpenAPI spec.

## Usage

Create client from environment variables with:

```go
import  "gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
c, err := clockworks.NewFromEnvironment()
```

Be sure `CLOCKWORKS_USERNAME`, `CLOCKSWORKS_TOKEN` and optionally
`CLOCKWORKS_BASE_URL` are set.

### Disclaimer

*This code is freely available for non-commercial use and is provided as-is with no warranty.*
