package main

import (
	"context"
	"log"
	"os"

	"gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

func main() {
	c, err := clockworks.NewFromEnvironment()
	if err != nil {
		log.Fatalf("failed to create client: %v", err)
	}
	if len(os.Args) != 2 {
		log.Fatal("usage: guess-network <fqdn>")
	}
	net, err := c.GuessPrimaryNetworkWithFQDN(context.Background(), os.Args[1])
	if err != nil {
		log.Fatalf("failed to create client: %v", err)
	}
	log.Printf("%+v", net)
}
