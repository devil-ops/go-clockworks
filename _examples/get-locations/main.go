package main

import (
	"context"
	"log"

	"gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

func main() {
	c, err := clockworks.NewFromEnvironment()
	if err != nil {
		log.Fatalf("failed to create client: %v", err)
	}

	locations, _, err := c.Location.List(context.Background())
	if err != nil {
		log.Fatalf("failed to request locations: %v", err)
	}
	for _, l := range locations {
		log.Printf("%+v", l)

		id := l.Id
		sl, _, err := c.Location.Get(context.Background(), id)
		if err != nil {
			log.Fatalf("failed to request location: %v", err)
		}
		log.Printf("%+v", sl)

		// List Networks on a location
		networks, _, err := c.Network.ListWithLocation(context.Background(), id)
		if err != nil {
			log.Fatalf("failed to request location networks: %v", err)
		}
		for _, n := range networks {
			log.Printf("%+v", n)
		}

		// List VMs on a location
		lvmc := clockworks.LocationListVMOpt{
			ShowDeleted:  false,
			ShowArchived: false,
			ShowLive:     true,
		}
		vms, _, err := c.VM.ListWithLocation(context.Background(), id, &lvmc)
		if err != nil {
			log.Fatalf("failed to request location networks: %v", err)
		}
		for _, n := range vms {
			log.Printf("%+v\n", n)
		}
		log.Print("")
	}
}
