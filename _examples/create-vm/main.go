package main

import (
	"context"
	"log"
	"os"

	"gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

func main() {
	if len(os.Args) != 2 {
		log.Fatal("usage: create-vm <fqdn>")
	}
	c := clockworks.New(clockworks.WithEnv())

	opt := clockworks.NewVMCreateOpts(
		clockworks.WithLocationID(1),
		clockworks.WithHostname(os.Args[1]),
		clockworks.WithOSID("ssiUbuntu-20"),
		clockworks.WithSysadminOption(clockworks.WorkHoursGeneralSupport),
		clockworks.WithPatchWindowID(clockworks.AnyPatchWindow),
		clockworks.WithFundCode("000-9332"),
		clockworks.WithPatchWindowID(2),
		clockworks.WithSkipFinalize(),
		clockworks.WithDisks([]int{50, 25}),
	)
	r, _, err := c.VM.Create(context.Background(), opt)
	if err != nil {
		log.Fatalf("failed to create VM: %v", err)
	}
	err = c.VMRequest.WaitForFinalStatus(context.Background(), r.VMRequestID)
	if err != nil {
		log.Fatalf("failed to wait for VM creation: %v", err)
	}
}
