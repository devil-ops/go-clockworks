package main

import (
	"context"
	"log"
	"os"
	"strconv"

	"gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

func main() {
	c, err := clockworks.NewFromEnvironment()
	if err != nil {
		log.Fatalf("failed to create client: %v", err)
	}
	if len(os.Args) != 3 {
		log.Fatal("usage: edit-cpu <fqdn> <cpu>")
	}
	var cpuCount int
	cpuCount, err = strconv.Atoi(os.Args[2])
	if err != nil {
		log.Fatal("CPU Count must be an integer")
	}
	vm, _, err := c.VM.GetWithFQDN(context.Background(), os.Args[1])
	if err != nil {
		log.Fatalf("failed to find VM: %v", err)
	}

	opt := &clockworks.VMEditOpts{
		CPU:             cpuCount,
		PoweroffAllowed: true,
	}
	r, _, err := c.VM.Edit(context.Background(), vm.Id, opt)
	if err != nil {
		log.Fatalf("failed to find VM: %v", err)
	}
	err = c.VMRequest.WaitForFinalStatus(context.Background(), r.VMRequestId)
	if err != nil {
		log.Fatalf("failed to find VM: %v", err)
	}
}
