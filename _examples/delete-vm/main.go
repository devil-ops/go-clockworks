package main

import (
	"context"
	"log"
	"os"
	"time"

	"gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

func main() {
	if len(os.Args) != 2 {
		log.Fatal("usage: delete-vm <fqdn>")
	}
	c := clockworks.New(clockworks.WithEnv())
	vm, _, err := c.VM.GetWithFQDN(context.Background(), os.Args[1])
	if err != nil {
		log.Fatalf("failed to find VM: %v", err)
	}

	now := time.Now()
	deleteAt := now.Add(time.Minute * 5)

	opt := &clockworks.VMDeleteOpts{
		ArchiveDeleteTime: &deleteAt,
	}
	r, _, err := c.VM.Delete(context.Background(), vm.ID, opt)
	if err != nil {
		log.Fatalf("failed to find VM: %v", err)
	}
	err = c.VMRequest.WaitForFinalStatus(context.Background(), r.VMRequestID)
	if err != nil {
		log.Fatalf("failed to find VM: %v", err)
	}
}
