package main

import (
	"context"
	"log"

	"gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

func main() {
	c, err := clockworks.NewFromEnvironment()
	if err != nil {
		log.Fatalf("failed to create client: %v", err)
	}
	log.Printf("%+v", c)

	items, _, err := c.VM.List(context.Background(), nil)
	if err != nil {
		log.Fatalf("failed to request vms: %v", err)
	}
	for _, l := range items {
		log.Printf("%+v", l)
	}
}
