package clockworks

import (
	"testing"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/require"
)

func TestContainsString(t *testing.T) {
	tests := []struct {
		s []string
		e string
		r bool
	}{
		{s: []string{"a", "b", "c"}, e: "a", r: true},
		{s: []string{"a", "b", "c"}, e: "d", r: false},
	}

	for _, test := range tests {
		got := containsString(test.s, test.e)
		require.Equal(t, test.r, got)
	}
}

func TestContainsInt(t *testing.T) {
	tests := []struct {
		s []int
		e int
		r bool
	}{
		{s: []int{1, 2, 3}, e: 2, r: true},
		{s: []int{1, 2, 3}, e: 4, r: false},
	}

	for _, test := range tests {
		got := containsInt(test.s, test.e)
		require.Equal(t, test.r, got)
	}
}

func TestMustParseURL(t *testing.T) {
	// Test good
	require.NotPanics(t, func() { mustParseURL("https://www.example.com") })
	require.NotNil(t, mustParseURL("https://www.example.com"))

	require.Panics(t, func() { mustParseURL("%+o") })
}

func TestMustNewRequest(t *testing.T) {
	require.NotPanics(t, func() { mustNewRequest("GET", "https://www.example.com", nil) })
	require.NotNil(t, mustNewRequest("GET", "https://www.example.com", nil))

	require.Panics(t, func() { mustNewRequest("GET", "%+o", nil) })
}

func TestReverseMap(t *testing.T) {
	m := map[string]string{
		"foo": "bar",
		"moo": "baz",
	}
	require.Equal(
		t,
		map[string]string{
			"bar": "foo",
			"baz": "moo",
		},
		reverseMap(m),
	)
}

func TestViperGet(t *testing.T) {
	v := viper.New()
	v.Set("username", "foo")

	got := viperOrPanic("username", "this is a panic", v)
	require.Equal(t, "foo", got)

	require.Panics(t, func() { viperOrPanic("never-exists", "this doesn't exist", v) })
}
