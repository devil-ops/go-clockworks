package clockworks

import (
	"testing"

	"github.com/spf13/cobra"
	"github.com/stretchr/testify/require"
)

func TestProductionLevelWithString(t *testing.T) {
	tests := map[string]struct {
		s       string
		w       ProductionLevel
		wantErr string
	}{
		"production": {
			s: "production",
			w: Production,
		},
		"testing": {
			s: "testing",
			w: Testing,
		},
		"development": {
			s: "development",
			w: Development,
		},
		"unknown-production-level": {
			s:       "Never-exists",
			wantErr: "ProductionLevel should be one of: [development production testing]",
		},
	}

	for desc, tt := range tests {
		tt := tt
		var pl ProductionLevel
		if tt.wantErr == "" {
			require.NoError(t, pl.Set(tt.s), desc)
			require.IsType(t, tt.w, pl, desc)
		} else {
			require.EqualError(t, pl.Set(tt.s), tt.wantErr, desc)
		}
	}
}

func TestProductionLevelCompletion(t *testing.T) {
	got, _ := ProductionLevelCompletion(&cobra.Command{}, []string{}, "")
	require.Equal(
		t,
		[]string{"development", "production", "testing"},
		got,
	)
}

func TestProductionLevelMarshal(t *testing.T) {
	l := Production
	got, err := l.MarshalJSON()
	require.NoError(t, err)
	require.Equal(t, `"production"`, string(got))
}
