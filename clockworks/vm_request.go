package clockworks

import (
	"context"
	"fmt"
	"net/http"
	"time"

	tea "github.com/charmbracelet/bubbletea"
)

// VMRequest holds all the information needed for a VMRequest
type VMRequest struct {
	APIUserNetid       string `json:"api_user_netid,omitempty"`
	ApproverID         int    `json:"approver_id,omitempty"`
	ApproverNetid      string `json:"approver_netid,omitempty"`
	Comments           string `json:"comments,omitempty"`
	CreatedAt          string `json:"created_at,omitempty"`
	DateRequested      string `json:"date_requested,omitempty"`
	DateStarted        string `json:"date_started,omitempty"`
	Hostname           string `json:"hostname,omitempty"`
	ID                 int    `json:"id,omitempty"`
	OldVMRequestInfoID int    `json:"old_vm_request_info_id,omitempty"`
	ScheduledStart     string `json:"scheduled_start,omitempty"`
	SensitiveData      bool   `json:"sensitive_data,omitempty"`
	SkipFinalize       bool   `json:"skip_finalize,omitempty"`
	SnRequestID        string `json:"sn_request_id,omitempty"`
	Status             string `json:"status,omitempty"`
	StepID             int    `json:"step_id,omitempty"`
	Type               string `json:"type,omitempty"`
	UpdatedAt          string `json:"updated_at,omitempty"`
	UserID             int    `json:"user_id,omitempty"`
	UserNetid          string `json:"user_netid,omitempty"`
	VMID               int    `json:"vm_id,omitempty"`
	VMRequestInfoID    int    `json:"vm_request_info_id,omitempty"`
}

// VMRequestResponse is the response to a VMRequest
type VMRequestResponse struct {
	SNRequestID     string   `json:"sn_request_id,omitempty"`
	VMRequestAPIURL string   `json:"vm_request_api_url,omitempty"`
	VMRequestID     int      `json:"vm_request_id,omitempty"`
	VMRequestWebURL string   `json:"vm_request_web_url,omitempty"`
	Errors          []string `json:"errors,omitempty"`
}

// VMRequestService contains all of the methods for a VMRequest
type VMRequestService interface {
	List(context.Context, *VMRequestListOpts) ([]VMRequest, *Response, error)
	ListWithVMId(context.Context, int, *VMRequestListOpts) ([]VMRequest, *Response, error)
	Get(context.Context, int) (*VMRequest, *Response, error)
	WaitForFinalStatusTUI(context.Context, int) error
	WaitForFinalStatus(context.Context, int) error
}

// VMRequestListOpts is the config for a VMRequestList
type VMRequestListOpts struct {
	Status string `url:"status,omitempty"`
}

// VMRequestServiceOp is the operator for the VMRequestService
type VMRequestServiceOp struct {
	client *Client
}

// waitReqStatus waits the built in duration of a given status. Also pass in a default to use if the status isn't found
func (s *VMRequestServiceOp) waitReqStatus(st string, d time.Duration) {
	if dur, ok := s.client.reqWaitTimes[st]; ok {
		time.Sleep(dur)
	} else {
		time.Sleep(d)
	}
}

// Get returns a specific VMRequest, given the ID
func (s *VMRequestServiceOp) Get(ctx context.Context, name int) (*VMRequest, *Response, error) {
	path := fmt.Sprintf("api/v1/vm_requests/%v", name)

	req := s.client.mustNewRequest(http.MethodGet, path, nil)

	// root := new(accountRoot)
	var item VMRequest
	resp, err := s.client.Do(ctx, req, &item)
	if err != nil {
		return nil, resp, err
	}

	return &item, resp, err
}

// List returns all VMRequests
func (s *VMRequestServiceOp) List(ctx context.Context, opt *VMRequestListOpts) ([]VMRequest, *Response, error) {
	if opt == nil {
		opt = &VMRequestListOpts{
			Status: "pending",
		}
	}
	path := "api/v1/vm_requests"
	path, err := addOptions(path, opt)
	if err != nil {
		return nil, nil, err
	}

	req := s.client.mustNewRequest(http.MethodGet, path, nil)

	// root := new(accountRoot)
	var items []VMRequest
	resp, err := s.client.Do(ctx, req, &items)
	if err != nil {
		return nil, resp, err
	}

	return items, resp, err
}

// ListWithVMId returns a list of VMRequests with a given VM ID
func (s *VMRequestServiceOp) ListWithVMId(ctx context.Context, id int, opt *VMRequestListOpts) ([]VMRequest, *Response, error) {
	if opt == nil {
		opt = &VMRequestListOpts{
			Status: "pending",
		}
	}
	path := fmt.Sprintf("api/v1/vms/%v/vm_requests", id)
	path, err := addOptions(path, opt)
	if err != nil {
		return nil, nil, err
	}

	req := s.client.mustNewRequest(http.MethodGet, path, nil)

	// root := new(accountRoot)
	var items []VMRequest
	resp, err := s.client.Do(ctx, req, &items)
	if err != nil {
		return nil, resp, err
	}

	return items, resp, err
}

// WaitForFinalStatusTUI - Blocks until a VMRequest is in a final status. Usually completed or errored in a TUI
func (s *VMRequestServiceOp) WaitForFinalStatusTUI(ctx context.Context, id int) error {
	p := tea.NewProgram(vmRequestStatusInitialModel(s.client, id, []string{"completed", "done", "failed", "error"}))
	if _, err := p.Run(); err != nil {
		return err
	}
	_, _, err := s.Get(ctx, id)
	if err != nil {
		return err
	}

	return nil
}

// WaitForFinalStatus just waits for a completed status
func (s *VMRequestServiceOp) WaitForFinalStatus(ctx context.Context, id int) error {
	for {
		r, _, err := s.Get(ctx, id)
		if err != nil {
			return err
		}
		if containsString([]string{"error", "failed", "done", "completed"}, r.Status) {
			break
		}

		// Wait for next status, but try not to hammer clockworks
		s.waitReqStatus(r.Status, time.Second*1)
	}
	return nil
}
