package clockworks

import (
	"fmt"
	"net/http"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestListContainers(t *testing.T) {
	// Create a new client
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/containers", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)

		content, _ := os.ReadFile("testdata/containers.json")
		fmt.Fprint(w, string(content))
	})

	items, _, err := client.Container.List(ctx)
	require.NoError(t, err)
	require.IsType(t, []Container{}, items)
	require.Greater(t, len(items), 0)
}

func TestGetContainer(t *testing.T) {
	// Create a new client
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/containers/foo", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)

		content, _ := os.ReadFile("testdata/container-foo.json")
		fmt.Fprint(w, string(content))
	})

	items, _, err := client.Container.Get(ctx, "foo")
	require.NoError(t, err)
	require.IsType(t, &Container{}, items)
}

func TestValidateContainer(t *testing.T) {
	setup()
	defer teardown()
	mux.HandleFunc("/api/v1/containers", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)

		content := `[{"name": "foo"}]`
		fmt.Fprint(w, content)
	})
	ts := []struct {
		n       string
		s       int
		wantErr bool
	}{
		{
			n:       "foo",
			s:       1,
			wantErr: false,
		},
		{
			n:       "",
			s:       1,
			wantErr: true,
		},
		// No container needed for non-self-admin
		{
			n:       "",
			s:       2,
			wantErr: false,
		},
	}
	for _, tt := range ts {
		if tt.wantErr {
			require.Error(t, NewContainer(tt.n).ValidateWithSysadminOption(SysadminOption(tt.s)))
		} else {
			require.NoError(t, NewContainer(tt.n).ValidateWithSysadminOption(SysadminOption(tt.s)))
		}
	}
}
