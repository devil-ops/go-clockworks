package clockworks

import (
	"fmt"
	"net/http"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestListLocations(t *testing.T) {
	// Create a new client
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/locations", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)

		content, _ := os.ReadFile("testdata/locations/all.json")
		fmt.Fprint(w, string(content))
	})

	items, _, err := client.Location.List(ctx)
	require.NoError(t, err)
	require.Greater(t, len(items), 0)
}

func TestGetLocation(t *testing.T) {
	// Create a new client
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/locations/1", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)

		content, _ := os.ReadFile("testdata/locations/1.json")
		fmt.Fprint(w, string(content))
	})

	item, _, err := client.Location.Get(ctx, 1)
	require.NoError(t, err)
	require.NotNil(t, item)
	require.Equal(t, 1, item.ID)
	require.Equal(t, BackupsAndDR, item.MaxAllowedBackups)
}

func TestGetLocationWithName(t *testing.T) {
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/locations", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)
		fileWriter(w, "testdata/locations/all.json")
	})

	item, _, err := client.Location.GetWithName(ctx, "ATC Lab Dev")
	require.NoError(t, err)
	require.NotNil(t, item)
	require.Equal(t, "ATC Lab Dev", item.DisplayName)
}

func TestListLocationWithNetwork(t *testing.T) {
	// Create a new client
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/networks/foo/locations", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)

		content, _ := os.ReadFile("testdata/networks-foo-locations.json")
		fmt.Fprint(w, string(content))
	})

	items, _, err := client.Location.ListWithNetwork(ctx, "foo")
	require.NoError(t, err)
	require.IsType(t, []Location{}, items)
}

func TestListLocationIds(t *testing.T) {
	// Create a new client
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/locations", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)

		content, _ := os.ReadFile("testdata/locations/all.json")
		fmt.Fprint(w, string(content))
	})

	items, _, err := client.Location.ListIDs(ctx)
	require.NoError(t, err)
	require.IsType(t, []int{}, items)
	require.Equal(t, []int{1, 7, 3}, items)
}

func TestListLocationDisplayNameMap(t *testing.T) {
	// Create a new client
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/locations", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)

		content, _ := os.ReadFile("testdata/locations/all.json")
		fmt.Fprint(w, string(content))
	})

	items, _, err := client.Location.DisplayNameMap(ctx)
	require.NoError(t, err)
	require.IsType(t, map[string]int{}, items)
	require.Equal(t, map[string]int{"ATC Lab": 1, "ATC Lab Dev": 7, "acme-s-7-9.esx.example.com": 3}, items)
}

func TestListLocationDisplayNames(t *testing.T) {
	// Create a new client
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/locations", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)

		content, _ := os.ReadFile("testdata/locations/all.json")
		fmt.Fprint(w, string(content))
	})

	items, _, err := client.Location.ListDisplayNames(ctx)
	require.NoError(t, err)
	require.IsType(t, []string{}, items)
	require.Contains(t, items, "ATC Lab Dev")
}

func TestCanBackup(t *testing.T) {
	tests := map[string]struct {
		l    Location
		want bool
	}{
		"max-none":       {l: Location{MaxAllowedBackups: NoBackups}, want: false},
		"max-backups":    {l: Location{MaxAllowedBackups: Backups}, want: true},
		"max-backups-dr": {l: Location{MaxAllowedBackups: BackupsAndDR}, want: true},
	}

	for desc, tt := range tests {
		tt := tt
		require.Equal(t, tt.want, tt.l.CanBackup(), desc)
	}
}

func TestCanDR(t *testing.T) {
	tests := map[string]struct {
		l    Location
		want bool
	}{
		"max-none":       {l: Location{MaxAllowedBackups: NoBackups}, want: false},
		"max-backups":    {l: Location{MaxAllowedBackups: Backups}, want: false},
		"max-backups-dr": {l: Location{MaxAllowedBackups: BackupsAndDR}, want: true},
	}

	for desc, tt := range tests {
		tt := tt
		require.Equal(t, tt.want, tt.l.CanDR(), desc)
	}
}

func TestInferBackup(t *testing.T) {
	tests := map[string]struct {
		l    Location
		want bool
	}{
		"min-none":       {l: Location{MinAllowedBackups: NoBackups}, want: false},
		"min-backups":    {l: Location{MinAllowedBackups: Backups}, want: true},
		"min-backups-dr": {l: Location{MinAllowedBackups: BackupsAndDR}, want: true},
	}

	for desc, tt := range tests {
		tt := tt
		require.Equal(t, tt.want, tt.l.InferredBackup(), desc)
	}
}

func TestInferDR(t *testing.T) {
	tests := map[string]struct {
		l    Location
		want bool
	}{
		"min-none":       {l: Location{MinAllowedBackups: NoBackups}, want: false},
		"min-backups":    {l: Location{MinAllowedBackups: Backups}, want: false},
		"min-backups-dr": {l: Location{MinAllowedBackups: BackupsAndDR}, want: true},
	}

	for desc, tt := range tests {
		tt := tt
		require.Equal(t, tt.want, tt.l.InferredDR(), desc)
	}
}
