package clockworks

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestWithCreatVM(t *testing.T) {
	require.EqualValues(
		t,
		&VMCreateOpts{
			ApplicationName:   "",
			Backups:           false,
			CloudInitUserData: "#foo",
			ContainerName:     "",
			CPU:               5,
			DisasterRecovery:  false,
			Disks:             DiskSet{VMDisk{DiskID: 0, SizeGB: 50}, VMDisk{DiskID: 1, SizeGB: 100}},
			FundCode:          "xyz",
			Hostname:          "",
			LocationID:        5,
			NetworkType:       1,
			OnBehalfOf:        "",
			OSID:              "linux-1.2.3",
			PatchWindowID:     -1,
			ProductionLevel:   0,
			RAM:               42,
			SecureProjectID:   0,
			SkipFinalize:      true,
			SysadminOptionID:  0,
			Subnet:            "",
			StorageType:       1,
			skipValidate:      true,
		},
		NewVMCreateOpts(
			WithoutValidate(),
			WithRAM[VMCreateOpts](42),
			WithCPU[VMCreateOpts](5),
			WithFundCode[VMCreateOpts]("xyz"),
			WithDisks[VMCreateOpts](NewDiskSet([]int{50, 100})),
			WithStorageType[VMCreateOpts](PlatinumStorage),
			withLocationID[VMCreateOpts](5),
			WithCloudInitUserData("#foo"),
			WithSkipFinalize(),
			WithNetworkType(PublicNetwork),
			WithPatchWindowID(AnyPatchWindow),
			WithOSID("linux-1.2.3"),
		),
	)
}

func TestWithEditVM(t *testing.T) {
	require.EqualValues(
		t,
		&VMEditOpts{
			RAM:              42,
			CPU:              5,
			SysadminOptionID: 0,
			FundCode:         "xyz",
			StorageType:      1,
			Disks: &DiskSet{
				VMDisk{DiskID: 0, SizeGB: 50},
				VMDisk{DiskID: 1, SizeGB: 100},
			},
			PoweroffAllowed: true,
		},
		NewVMEditOpts(
			WithRAM[VMEditOpts](42),
			WithCPU[VMEditOpts](5),
			WithFundCode[VMEditOpts]("xyz"),
			WithDisks[VMEditOpts](NewDiskSet([]int{50, 100})),
			WithStorageType[VMEditOpts](PlatinumStorage),
			WithPoweroffAllowed(),
		),
	)
}

func TestNewVMListOptsDefaults(t *testing.T) {
	require.EqualValues(
		t,
		&VMListOpts{
			HideDetails: true,
			ShowLive:    true,
		},
		NewVMListOpts(),
	)
}

func TestNewVMListOpts(t *testing.T) {
	require.EqualValues(
		t,
		&VMListOpts{
			HideDetails:   false,
			IPAddress:     "1.2.3.4",
			Name:          "hi",
			ShowDeleted:   true,
			ShowArchived:  true,
			ShowLive:      false,
			LocationID:    5,
			SysadminnedBy: "joe",
		},
		NewVMListOpts(
			WithDetails(),
			WithName("hi"),
			WithSysadminnedBy("joe"),
			WithDeleted(),
			WithArchived(),
			WithoutLive(),
			withLocationID[VMListOpts](5),
			WithIPAddress("1.2.3.4"),
		),
	)
}

func TestNewVMDeleteOpts(t *testing.T) {
	require.NotNil(t, NewVMDeleteOpts())
	require.NotNil(t, NewVMDeleteOpts(WithArchiveDeleteTime(time.Now().Add(time.Hour))))
}

func TestNewVMCreateOptsBackups(t *testing.T) {
	// Minimum is 'Backups'
	got := NewVMCreateOpts(
		WithLocation[VMCreateOpts](Location{
			ID:                1,
			MinAllowedBackups: Backups,
		}),
		WithoutValidate(),
	)
	require.EqualValues(t, 1, got.LocationID)
	require.Equal(t, true, got.Backups)
	require.Equal(t, false, got.DisasterRecovery)

	// Minimum is 'BackupsAndDr'
	got = NewVMCreateOpts(
		WithLocation[VMCreateOpts](Location{
			ID:                2,
			MinAllowedBackups: BackupsAndDR,
		}),
		WithoutValidate(),
	)
	require.EqualValues(t, 2, got.LocationID)
	require.Equal(t, true, got.Backups)
	require.Equal(t, true, got.DisasterRecovery)

	// Minimum is 'NoBackups
	got = NewVMCreateOpts(
		WithLocation[VMCreateOpts](Location{
			ID:                3,
			MinAllowedBackups: NoBackups,
		}),
		WithoutValidate(),
	)
	require.EqualValues(t, 3, got.LocationID)
	require.Equal(t, false, got.Backups)
	require.Equal(t, false, got.DisasterRecovery)
}

func TestWithStartTime(t *testing.T) {
	// Use a real timestamp
	st := time.Now().Add(24 * time.Hour)
	got := NewVMEditOpts(
		WithStartTime[VMEditOpts](st),
	)
	require.Equal(t, st, *got.StartTime)

	// Use a string with localtime
	got = NewVMEditOpts(
		WithStartTime[VMEditOpts]("2025-01-05 08:00:00"),
	)
	require.Equal(t, time.Date(2025, time.January, 5, 8, 0, 0, 0, time.Local), *got.StartTime)

	// Use a string with UTC
	got = NewVMEditOpts(
		WithStartTime[VMEditOpts]("2025-01-05 08:00:00 UTC"),
	)
	require.Equal(t, time.Date(2025, time.January, 5, 8, 0, 0, 0, time.UTC), *got.StartTime)
}
