package clockworks

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"sort"
	"strconv"
	"strings"
)

// AnyPatchWindow picks any available out of hours window
const AnyPatchWindow int = -1

// PatchWindow represents a Patchmonkey Window from Clockworks
type PatchWindow struct {
	ID   float64 `json:"id,omitempty"`
	Name string  `json:"name,omitempty"`
}

// ValidatePatchWindowWithSysadminOption ensures these work together as expected
func ValidatePatchWindowWithSysadminOption(p int, s SysadminOption) error {
	if (s == 1) && (p != 0) {
		return errors.New("only specify a patch window when not a self-admin request")
	}
	return nil
}

// PatchWindowService contains all the PatchWindow methods
type PatchWindowService interface {
	List(context.Context) ([]PatchWindow, *Response, error)
	ListIDs(context.Context) ([]int, *Response, error)
	ShellComplete(context.Context, string) []string
}

// PatchWindowServiceOp is the operator for the PatchWindowService
type PatchWindowServiceOp struct {
	client *Client
}

// ListIDs returns a list of all PatchWindow IDs
func (s *PatchWindowServiceOp) ListIDs(ctx context.Context) ([]int, *Response, error) {
	ret := []int{}
	pws, resp, err := s.List(ctx)
	if err != nil {
		return nil, resp, err
	}
	for _, i := range pws {
		ret = append(ret, int(i.ID))
	}
	return ret, resp, nil
}

// List returns all PatchWindows
func (s *PatchWindowServiceOp) List(ctx context.Context) ([]PatchWindow, *Response, error) {
	path := "api/v1/patch_windows"

	req := s.client.mustNewRequest(http.MethodGet, path, nil)

	// root := new(accountRoot)
	var items []PatchWindow
	resp, err := s.client.Do(ctx, req, &items)
	if err != nil {
		return nil, resp, err
	}

	return items, resp, err
}

// ShellComplete provides shell completion for VM stuff
func (s *PatchWindowServiceOp) ShellComplete(ctx context.Context, filter string) []string {
	items, _, err := s.List(ctx)
	if err != nil {
		panic(err)
		// return
	}
	var ret []string
	for _, i := range items {
		if strings.Contains(i.Name, filter) {
			ret = append(ret, fmt.Sprintf("%v\t%v", i.ID, i.Name))
		}
	}
	// Pass in our list and a func to compare values
	sort.Slice(ret, func(i, j int) bool {
		numA, _ := strconv.Atoi(strings.Split(ret[i], "\t")[0])
		numB, _ := strconv.Atoi(strings.Split(ret[j], "\t")[0])
		return numA < numB
	})
	return ret
}
