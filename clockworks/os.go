package clockworks

import (
	"context"
	"fmt"
	"net/http"
	"sort"
	"strings"
)

// OS Represents an Operating Systems from Clockworks
type OS struct {
	APIOnly          bool   `json:"api_only,omitempty"`
	Family           string `json:"family,omitempty"`
	FullName         string `json:"full_name,omitempty"`
	ID               int    `json:"id,omitempty"`
	InitScript       string `json:"init_script,omitempty"`
	Movement         string `json:"movement,omitempty"`
	OSId             string `json:"os_id,omitempty"`
	SCCMCollectionID string `json:"sccm_collection_id,omitempty"`
	SSIManaged       bool   `json:"ssi_managed,omitempty"`
	SSIOnly          bool   `json:"ssi_only,omitempty"`
	Template         string `json:"template,omitempty"`
}

// OSService holds all of the OS related methods
type OSService interface {
	List(context.Context) ([]OS, *Response, error)
	ListIDs(context.Context) ([]string, *Response, error)
	Get(context.Context, string) (*OS, *Response, error)
	ShellComplete(context.Context, string) []string
}

// OSServiceOp is the operator for the OSService
type OSServiceOp struct {
	client *Client
}

// ListIDs returns a list of IDs for the OSes
func (s *OSServiceOp) ListIDs(ctx context.Context) ([]string, *Response, error) {
	var ret []string
	l, resp, err := s.List(ctx)
	if err != nil {
		return nil, resp, err
	}
	for _, i := range l {
		ret = append(ret, i.OSId)
	}
	return ret, resp, nil
}

// List returns all the OSes
func (s *OSServiceOp) List(ctx context.Context) ([]OS, *Response, error) {
	path := "api/v1/oses"

	req := s.client.mustNewRequest(http.MethodGet, path, nil)

	// root := new(accountRoot)
	var items []OS
	resp, err := s.client.Do(ctx, req, &items)
	if err != nil {
		return nil, resp, err
	}

	return items, resp, err
}

// Get returns a specific OS based on the given name
func (s *OSServiceOp) Get(ctx context.Context, name string) (*OS, *Response, error) {
	path := fmt.Sprintf("api/v1/oses/%v", name)

	req := s.client.mustNewRequest(http.MethodGet, path, nil)

	// root := new(accountRoot)
	var item OS
	resp, err := s.client.Do(ctx, req, &item)
	if err != nil {
		return nil, resp, err
	}

	return &item, resp, err
}

/*
// ValidateOSId validates an OS
func (s *OSServiceOp) ValidateOSId(ctx context.Context, target interface{}) error {
	id := target.(string)

	if id == "" {
		return fmt.Errorf("OS id must not be empty")
	}
	validIds, _, err := s.ListIds(ctx)
	if err != nil {
		return err
	}
	if !containsString(validIds, id) {
		return fmt.Errorf("invalid OS ID: %v. Valid OSIds: %v", id, validIds)
	}

	return nil
}
*/

// ShellComplete provides shell complete functionality for Locations
func (s *OSServiceOp) ShellComplete(ctx context.Context, filter string) []string {
	items, _, err := s.List(ctx)
	if err != nil {
		panic(err)
	}
	var ret []string
	for _, i := range items {
		if strings.Contains(strings.ToLower(i.OSId), strings.ToLower(filter)) {
			ret = append(ret, fmt.Sprintf("%v\t%v", i.OSId, i.FullName))
		}
	}
	sort.Strings(ret)
	return ret
}
