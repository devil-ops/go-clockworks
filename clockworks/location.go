package clockworks

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"sort"
	"strings"
)

// AllowedBackups represents a range of backup types that a Location can have
type AllowedBackups int

const (
	// NoBackups is...no backups!
	NoBackups AllowedBackups = iota
	// Backups is on-site VM backups
	Backups
	// BackupsAndDR is on-site and offsite
	BackupsAndDR
)

// UnmarshalJSON turns the int in to an actual AllowedBackups type
func (a *AllowedBackups) UnmarshalJSON(data []byte) error {
	var v interface{}
	if err := json.Unmarshal(data, &v); err != nil {
		return err
	}

	// This gets Unmarshalled to float64 by default, so just go with it
	is, ok := v.(float64)
	if !ok {
		panic("Uh, not sure what to do here with this AllowedBackup")
	}

	switch is {
	case 0:
		*a = NoBackups
	case 1:
		*a = Backups
	case 2:
		*a = BackupsAndDR
	default:
		panic(fmt.Sprintf("Unexpected result for AllowedBackups: %v", is))
	}

	return nil
}

// Location represents a Location item in Clockworks
type Location struct {
	ID                int            `json:"id"`
	ParentLocation    int            `json:"parent_location,omitempty"`
	MemGBCapacity     int            `json:"mem_gb_capacity,omitempty"`
	MemGBUsed         int            `json:"mem_gb_used,omitempty"`
	CPUUsed           int            `json:"cpu_used,omitempty"`
	MinAllowedBackups AllowedBackups `json:"min_allowed_backups,omitempty"`
	MaxAllowedBackups AllowedBackups `json:"max_allowed_backups,omitempty"`
	IsCluster         bool           `json:"is_cluster,omitempty"`
	Type              string         `json:"type,omitempty"`
	VCenterFQDN       string         `json:"vcenter_fqdn,omitempty"`
	DisplayName       string         `json:"display_name,omitempty"`
	ClusterName       string         `json:"cluster_name,omitempty"`
	ChildLocations    []interface{}  `json:"child_locations,omitempty"`
	Datastores        []string       `json:"datastores,omitempty"`
	Tags              []string       `json:"tags,omitempty"`
}

// InferredBackup returns true if the location has a >= min_allowed_backups of 1
func (l *Location) InferredBackup() bool {
	return l.MinAllowedBackups >= Backups
}

// InferredDR returns true if the location has a >= min_allowed_backups of 2
func (l *Location) InferredDR() bool {
	return l.MinAllowedBackups >= BackupsAndDR
}

// CanBackup returns a boolean representing if normal backups can be performed
// on VMSs in this location
func (l *Location) CanBackup() bool {
	return l.MaxAllowedBackups >= Backups
}

// CanDR returns a boolean representing if disaster recovery can be performed
// on VMS in this location
func (l *Location) CanDR() bool {
	return l.MaxAllowedBackups >= BackupsAndDR
}

// LocationListVMOpt are the options that can be passed to the LocationList service
type LocationListVMOpt struct {
	ShowDeleted  bool `url:"show_deleted"`
	ShowArchived bool `url:"show_archived"`
	ShowLive     bool `url:"show_live"`
}

// LocationService holds on the methods for interacting with the Location endpoint in Clockworks
type LocationService interface {
	List(context.Context) ([]Location, *Response, error)
	ListIDs(context.Context) ([]int, *Response, error)
	ListWithNetwork(context.Context, string) ([]Location, *Response, error)
	ListDisplayNames(context.Context) ([]string, *Response, error)
	Get(context.Context, int) (*Location, *Response, error)
	GetWithName(context.Context, string) (*Location, *Response, error)
	DisplayNameMap(context.Context) (map[string]int, *Response, error)
	ShellComplete(context.Context, string) []string
}

// LocationServiceOp is the operator for the LocationServic
type LocationServiceOp struct {
	client *Client
	cache  []Location
}

// fetchCache is just a little helper to populate the location cache
func (s *LocationServiceOp) fetchCache(ctx context.Context) (*Response, error) {
	var resp *Response
	if s.cache == nil {
		var err error
		_, resp, err = s.List(ctx)
		if err != nil {
			return resp, err
		}
	}
	return resp, nil
}

// ListDisplayNames returns a list of the display names for a location
func (s *LocationServiceOp) ListDisplayNames(ctx context.Context) ([]string, *Response, error) {
	resp, err := s.fetchCache(ctx)
	if err != nil {
		return nil, resp, err
	}
	var res []string
	for _, i := range s.cache {
		res = append(res, i.DisplayName)
	}
	sort.Strings(res)
	return res, resp, nil
}

// ListIDs returns just the ids for the locations
func (s *LocationServiceOp) ListIDs(ctx context.Context) ([]int, *Response, error) {
	var res []int
	l, resp, err := s.List(ctx)
	if err != nil {
		return nil, resp, err
	}
	for _, i := range l {
		res = append(res, i.ID)
	}
	return res, resp, nil
}

// List returns a list of all locations
func (s *LocationServiceOp) List(ctx context.Context) ([]Location, *Response, error) {
	path := "api/v1/locations"

	req := s.client.mustNewRequest(http.MethodGet, path, nil)

	var locations []Location
	resp, err := s.client.Do(ctx, req, &locations)
	if err != nil {
		return nil, resp, err
	}

	// Do some lazy caching
	s.cache = locations

	return locations, resp, err
}

// Get returns a single Location using it's ID
func (s *LocationServiceOp) Get(ctx context.Context, id int) (*Location, *Response, error) {
	path := fmt.Sprintf("api/v1/locations/%v", id)

	req := s.client.mustNewRequest(http.MethodGet, path, nil)

	// root := new(accountRoot)
	var location Location
	resp, err := s.client.Do(ctx, req, &location)
	if err != nil {
		return nil, resp, err
	}

	return &location, resp, err
}

// GetWithName returns a single location using it's Name
func (s *LocationServiceOp) GetWithName(ctx context.Context, name string) (*Location, *Response, error) {
	resp, err := s.fetchCache(ctx)
	if err != nil {
		return nil, resp, err
	}
	for _, item := range s.cache {
		if item.DisplayName == name {
			return &item, resp, nil
		}
	}
	return nil, resp, errors.New("no matching locations found")
}

// ListWithNetwork returns the Locations for a given network
func (s *LocationServiceOp) ListWithNetwork(ctx context.Context, name string) ([]Location, *Response, error) {
	path := fmt.Sprintf("api/v1/networks/%v/locations", name)

	req := s.client.mustNewRequest(http.MethodGet, path, nil)

	// root := new(accountRoot)
	var item []Location
	resp, err := s.client.Do(ctx, req, &item)
	if err != nil {
		return nil, resp, err
	}

	return item, resp, err
}

// DisplayNameMap returns a map of the locations, using the DisplayName as the key and the ID as the value
func (s *LocationServiceOp) DisplayNameMap(ctx context.Context) (map[string]int, *Response, error) {
	var resp *Response
	var err error
	if s.cache == nil {
		_, resp, err = s.List(ctx)
		if err != nil {
			return nil, resp, err
		}
	}

	m := make(map[string]int, len(s.cache))
	for _, i := range s.cache {
		m[i.DisplayName] = i.ID
	}
	return m, resp, err
}

// ShellComplete provides shell complete functionality for Locations
func (s *LocationServiceOp) ShellComplete(ctx context.Context, filter string) []string {
	items, _, err := s.List(ctx)
	if err != nil {
		panic(err)
	}
	var ret []string
	for _, i := range items {
		if strings.Contains(strings.ToLower(i.DisplayName), strings.ToLower(filter)) {
			ret = append(ret, fmt.Sprintf("%v\t%v", i.DisplayName, i.VCenterFQDN))
		}
	}
	sort.Strings(ret)
	return ret
}
