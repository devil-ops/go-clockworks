package clockworks

import (
	"time"

	"github.com/araddon/dateparse"
)

/*
The functions in this file are based on the functional options pattern, with
generics. This document describes the process behind them:

https://golang.design/research/generic-option/

When using an option that can apply to multiple types (Create and Edit for example), the syntax stuttors. For example:

Instead of:

	NewVMCreateOpts(WithoutValidate(), WithRAM(42))

This must be used:

	NewVMCreateOpts(WithoutValidate(), WithRAM[VMCreateOpts](42))

There is a proposal to fix this here:

	https://github.com/golang/go/issues/52272
*/

// Option is a generic that we can pass in to things like VM create/edit/etc
type Option[T VMCreateOpts | VMEditOpts | VMListOpts | VMDeleteOpts] func(*T)

// WithSecureProjectID sets the secure_project_id in a request
func WithSecureProjectID[T VMCreateOpts](c int) Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMCreateOpts:
			x.SecureProjectID = c
		default:
			panic("unexpected usage of WithSecureProjectID")
		}
	}
}

// WithCPU sets the CPU in a request
func WithCPU[T VMCreateOpts | VMEditOpts](c int) Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMCreateOpts:
			x.CPU = CPU(c)
		case *VMEditOpts:
			x.CPU = CPU(c)
		default:
			panic("unexpected usage of WithCPU")
		}
	}
}

// WithRAM sets the ram in a request
func WithRAM[T VMCreateOpts | VMEditOpts](r int) Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMCreateOpts:
			x.RAM = RAM(r)
		case *VMEditOpts:
			x.RAM = RAM(r)
		default:
			panic("unexpected usage of WithRAM")
		}
	}
}

// WithProductionLevel sets the application for a new request
func WithProductionLevel[T VMCreateOpts](f ProductionLevel) Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMCreateOpts:
			x.ProductionLevel = f
		default:
			panic("unexpected usage of WithProductionLevel")
		}
	}
}

// WithOnBehalfOf sets the on_behalf_of for a new request
func WithOnBehalfOf[T VMCreateOpts](f string) Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMCreateOpts:
			x.OnBehalfOf = f
		default:
			panic("unexpected usage of WithOnBehalfOf")
		}
	}
}

// WithApplication sets the application for a new request
func WithApplication[T VMCreateOpts](f string) Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMCreateOpts:
			x.ApplicationName = f
		default:
			panic("unexpected usage of WithApplication")
		}
	}
}

// WithFundCode sets the fund_code for a new request
func WithFundCode[T VMCreateOpts | VMEditOpts](f string) Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMCreateOpts:
			x.FundCode = FundCode(f)
		case *VMEditOpts:
			x.FundCode = FundCode(f)
		default:
			panic("unexpected usage of WithFundCode")
		}
	}
}

// WithContainer sets the VMware container for the given VM
func WithContainer[T VMCreateOpts](f string) Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMCreateOpts:
			x.ContainerName = f
		default:
			panic("unexpected usage of WithContainer")
		}
	}
}

// WithSubnet sets the subnet for the given VM
func WithSubnet[T VMCreateOpts](f string) Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMCreateOpts:
			x.Subnet = f
		default:
			panic("unexpected usage of WithSubnet")
		}
	}
}

// WithoutValidate skips any attempt to validate the request before it is sent
// on to Clockworks. Useful for less wordy testing, or if the validation logic
// breaks before we can ship a fix
func WithoutValidate[T VMCreateOpts]() Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMCreateOpts:
			x.skipValidate = true
		default:
			panic("unexpected use of WithSkipValidate")
		}
	}
}

// WithDisks sets the initial disk sizes for a new VM request
func WithDisks[T VMCreateOpts | VMEditOpts](disks DiskSet) Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMCreateOpts:
			x.Disks = disks
		case *VMEditOpts:
			x.Disks = &disks
		}
	}
}

// WithStorageType sets the storage_type in a new request
func WithStorageType[T VMCreateOpts | VMEditOpts](s StorageType) Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMCreateOpts:
			x.StorageType = s
		case *VMEditOpts:
			x.StorageType = s
		default:
			panic("unexpected use of WithStorageType")
		}
	}
}

// WithSysadminOption sets the sysadmin_option_id in a new request
func WithSysadminOption[T VMCreateOpts | VMEditOpts](s SysadminOption) Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMCreateOpts:
			x.SysadminOptionID = s
		case *VMEditOpts:
			x.SysadminOptionID = s
		default:
			panic("unexpected use of WithSysadminOption")
		}
	}
}

// WithPoweroffAllowed will set poweroff_allowed to true in a VM request
func WithPoweroffAllowed[T VMEditOpts]() Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMEditOpts:
			x.PoweroffAllowed = true
		default:
			panic("unexpected use of WithPoweroffAllowed")
		}
	}
}

// WithHostname sets a hostname in the VMCreateOpts
func WithHostname[T VMCreateOpts](h string) Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMCreateOpts:
			x.Hostname = Hostname(h)
		default:
			panic("unexepected use of WithHostname")
		}
	}
}

// WithCloudInitUserData sets the cloud_init_user_data in a new request
func WithCloudInitUserData[T VMCreateOpts](c string) Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMCreateOpts:
			x.CloudInitUserData = CloudInitUserData(c)
		default:
			panic("unexpected use of WithCloudInitUserData")
		}
	}
}

// WithBackups sets the backups value to 'true'
func WithBackups[T VMCreateOpts]() Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMCreateOpts:
			x.Backups = true
		default:
			panic("unexpected use of WithBackups")
		}
	}
}

// WithDisasterRecovery sets the disaster_recovery value to 'true'
func WithDisasterRecovery[T VMCreateOpts]() Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMCreateOpts:
			x.DisasterRecovery = true
		default:
			panic("unexpected use of WithBackups")
		}
	}
}

// WithSkipFinalize sets the skip_finalize value to 'true'
func WithSkipFinalize[T VMCreateOpts]() Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMCreateOpts:
			x.SkipFinalize = true
		default:
			panic("unexpected use of WithSkipFinalize")
		}
	}
}

// WithNetworkType specifies the network_type for a new request
func WithNetworkType[T VMCreateOpts](n NetworkType) Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMCreateOpts:
			x.NetworkType = n
		default:
			panic("unexpected use of WithNetworkType")
		}
	}
}

// WithPatchWindowID sets the patch_window_id in a new request
func WithPatchWindowID[T VMCreateOpts](p int) Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMCreateOpts:
			x.PatchWindowID = p
		default:
			panic("unexpected use of WithPatchWindowID")
		}
	}
}

// WithLocation specifies the location of a new VM. This will also set the
// backup and disasaster_recovery flags based off of the locations capabilities
func WithLocation[T VMCreateOpts | VMListOpts](l Location) Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMCreateOpts:
			x.Backups = l.InferredBackup()
			x.DisasterRecovery = l.InferredDR()
			x.LocationID = LocationID(l.ID)
		case *VMListOpts:
			x.LocationID = LocationID(l.ID)
		default:
			panic("unexpected use of WithLocationID")
		}
	}
}

// withLocationID specifies the location of a new VM This should only be used as
// a testing helper. Use of WithLocation is preferred, as it can also do
// inference on the backup and disaster recovery options for the given location
func withLocationID[T VMCreateOpts | VMListOpts](i int) Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMCreateOpts:
			x.LocationID = LocationID(i)
		case *VMListOpts:
			x.LocationID = LocationID(i)
		default:
			panic("unexpected use of WithLocationID")
		}
	}
}

// WithOSID sets a os_id in the VMCreateOpts
func WithOSID[T VMCreateOpts](h string) Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMCreateOpts:
			x.OSID = OSID(h)
		default:
			panic("unexpected use of WithOSID")
		}
	}
}

// WithDetails sets hide_details to false in a VM listing
func WithDetails[T VMListOpts]() Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMListOpts:
			x.HideDetails = false
		default:
			panic("unexpected use of WithDetails")
		}
	}
}

// WithName sets the name for a filter option
func WithName[T VMListOpts](n string) Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMListOpts:
			x.Name = n
		default:
			panic("unexpected use of WithName")
		}
	}
}

// WithSysadminnedBy sets the sysadminned_by for a filter option
func WithSysadminnedBy[T VMListOpts](n string) Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMListOpts:
			x.SysadminnedBy = n
		default:
			panic("unexpected use of WithSysadminnedBy")
		}
	}
}

// WithDeleted sets the show_deleted to true for a filter option
func WithDeleted[T VMListOpts]() Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMListOpts:
			x.ShowDeleted = true
		default:
			panic("unexpected use of WithDeleted")
		}
	}
}

// WithArchived sets the show_archived to true for a filter option
func WithArchived[T VMListOpts]() Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMListOpts:
			x.ShowArchived = true
		default:
			panic("unexpected use of WithArchived")
		}
	}
}

// WithoutLive sets the show_live to false for a filter option
func WithoutLive[T VMListOpts]() Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMListOpts:
			x.ShowLive = false
		default:
			panic("unexpected use of WithoutLive")
		}
	}
}

// WithIPAddress sets the ipaddress for a filter option
func WithIPAddress[T VMListOpts](n string) Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMListOpts:
			x.IPAddress = n
		default:
			panic("unexpected use of WithIPAddress")
		}
	}
}

// WithArchiveDeleteTime sets the archive_delete_time for a delete request
func WithArchiveDeleteTime[T VMDeleteOpts](n time.Time) Option[T] {
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMDeleteOpts:
			x.ArchiveDeleteTime = &n
		default:
			panic("unexpected use of WithArchiveDeleteTime")
		}
	}
}

// WithDeleteArchiveAfter waits the given duration after a VM is deleted, before starting the archive deletion
func WithDeleteArchiveAfter(n time.Duration) Option[VMDeleteOpts] {
	return func(o *VMDeleteOpts) {
		if o.StartTime == nil {
			ti := time.Now().Add(n)
			o.ArchiveDeleteTime = &ti
		} else {
			ti := o.StartTime.Add(n)
			o.ArchiveDeleteTime = &ti
		}
	}
}

// WithStartTime sets the start_time for a request
func WithStartTime[T VMEditOpts | VMCreateOpts | VMDeleteOpts, V time.Time | string](n V) Option[T] {
	var ti time.Time
	var t V
	switch any(t).(type) {
	case time.Time:
		ti = any(n).(time.Time)
	case string:
		var err error
		ti, err = dateparse.ParseLocal(any(n).(string))
		if err != nil {
			panic(err)
		}
	default:
		panic("unexpected type used with WithStartTime")
	}
	return func(o *T) {
		switch x := any(o).(type) {
		case *VMEditOpts:
			x.StartTime = &ti
		case *VMCreateOpts:
			x.StartTime = &ti
		case *VMDeleteOpts:
			x.StartTime = &ti
		default:
			panic("unexpected use of WithStartTime")
		}
	}
}
