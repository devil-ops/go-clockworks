package clockworks

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/spf13/cobra"
)

const (
	vmPath = "api/v1/vms"
)

// Validator is an interface that describes a...Validator
type Validator interface {
	Validate() error
}

// LocationID is the ID of the location for a VM
type LocationID int

// Validate ensures this is set to something > 0
func (l LocationID) Validate() error {
	if l <= 0 {
		return errors.New("location_id must be greater than 0")
	}
	return nil
}

// CPU is the CPU count for a host
type CPU int

// Validate ensures this is set to something > 0
func (c CPU) Validate() error {
	if c <= 0 {
		return errors.New("cpu must be greater than 0")
	}
	return nil
}

// RAM is the amount of RAM for a machine
type RAM int

// Validate ensures this is set to something > 0
func (r RAM) Validate() error {
	if r <= 0 {
		return errors.New("ram must be greater than 0")
	}
	return nil
}

// CloudInitUserData is a non empty string
type CloudInitUserData string

// Validate ensures this is set to a non empty string
func (r CloudInitUserData) Validate() error {
	if r == "" {
		return errors.New("cloud-init must be be set")
	}
	return nil
}

// FundCode is a the financial piece of a VM
type FundCode string

// Validate ensures this is set to a non empty string
func (r FundCode) Validate() error {
	if r == "" {
		return errors.New("fund_code must be be set")
	}
	return nil
}

// OSID is the ID of a Operating System for a new VM
type OSID string

// Validate ensures this is set to a non empty string
func (o OSID) Validate() error {
	if o == "" {
		return errors.New("os_id must be be set")
	}
	return nil
}

// SysadminOption is the type of support for a given VM
//
//	[1 == self-admin, 2 == 8/5, 3 == 24/7, 4 == 8/5 web, 5 == 24/7 web]
type SysadminOption int

// SysadminOptionCompletion returns shell completion for the sysadmin options
func SysadminOptionCompletion(_ *cobra.Command, _ []string, _ string) ([]string, cobra.ShellCompDirective) {
	return []string{
		"self-admin\tSelf Administration (Customer responsible for OS install)",
		"8/5\t8/5 Standard OIT-SI OS System Administration",
		"24/7\t24/7 Standard OIT-SI OS System Administration",
		"8/5-web\t8/5 Webhosting 🕸️ OIT-SI System Administration",
		"24/7-web\t24/7 Webhosting 🕸️ OIT-SI System Administration️",
	}, cobra.ShellCompDirectiveDefault
}

const (
	// NullSupport is the unset value
	NullSupport SysadminOption = iota
	// SelfAdminSupport is user supported, no ssi
	SelfAdminSupport
	// WorkHoursGeneralSupport is general 8-5 support
	WorkHoursGeneralSupport
	// FullGeneralSupport is 24/7 general support
	FullGeneralSupport
	// WorkHoursWebSupport is web supported 8-5
	WorkHoursWebSupport
	// FullWebSupport is 24/7 web support
	FullWebSupport
)

// Validate ensures this is set
func (s SysadminOption) Validate() error {
	if s == 0 {
		return errors.New("must set sysadmin_option")
	}
	return nil
}

// String returns the string representing a given SupportOption
func (s SysadminOption) String() string {
	m := reverseMap(sysadminMap)
	if v, ok := m[s]; ok {
		return v
	}
	panic("unexpected use of SysadminOption.String()")
}

var sysadminMap = map[string]SysadminOption{
	"":           NullSupport,
	"self-admin": SelfAdminSupport,
	"8/5":        WorkHoursGeneralSupport,
	"24/7":       FullGeneralSupport,
	"8/5-web":    WorkHoursWebSupport,
	"24/7-web":   FullWebSupport,
}

// Set helps fulfill the pflag.Value interface
func (s *SysadminOption) Set(v string) error {
	if v, ok := sysadminMap[v]; ok {
		*s = v
		return nil
	}
	return errors.New(sysadminOptionHelp())
}

func sysadminOptionHelp() string {
	opts := []string{}
	for k := range sysadminMap {
		if k != "" {
			opts = append(opts, k)
		}
	}
	return fmt.Sprintf("Available SysadminOptions: %v", opts)
}

// Type is used in the help for pflag.Value
func (s *SysadminOption) Type() string {
	return "SysadminOption"
}

// StorageType represents one of the storage classes in Clockworks
type StorageType int

// StorageTypeCompletion returns shell completion for the storage options
func StorageTypeCompletion(_ *cobra.Command, _ []string, _ string) ([]string, cobra.ShellCompDirective) {
	return []string{
		"platinum\t🥇 Top of the line super fast storage",
		"silver\t🥈 Really fast storage",
		"bronze\t🥉 Still pretty fast, but the slowest option",
	}, cobra.ShellCompDirectiveDefault
}

// Type is used in the help for pflag.Value
func (s *StorageType) Type() string {
	return "StorageType"
}

const (
	// NullStorageType is the unset Storage Type
	NullStorageType StorageType = iota
	// PlatinumStorage is the all flash array
	PlatinumStorage
	// SilverStorage is the 'fast' array
	SilverStorage
	// BronzeStorage is 'slow', but actually still pretty fast
	BronzeStorage
)

// MarshalJSON ensures that json conversions use the string value here, not the int value
func (s *StorageType) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("\"%v\"", s.String())), nil
}

var storageMap = map[string]StorageType{
	"platinum": PlatinumStorage,
	"silver":   SilverStorage,
	"bronze":   BronzeStorage,
	"":         NullStorageType,
}

func (s StorageType) String() string {
	m := reverseMap(storageMap)
	if v, ok := m[s]; ok {
		return v
	}
	panic("invalid storagetype")
}

// Set helps fulfill the pflag.Value interface
func (s *StorageType) Set(v string) error {
	if v, ok := storageMap[v]; ok {
		*s = v
		return nil
	}
	return fmt.Errorf("StorageType should be one of: %v", nonEmptyKeys(storageMap))
}

// StorageTypeWithString returns a storage type using the given string.
func StorageTypeWithString(s string) StorageType {
	if v, ok := storageMap[s]; ok {
		return v
	}
	panic(fmt.Sprintf("unknown storage type: %v", s))
}

// Validate returns an error if empty
func (s StorageType) Validate() error {
	if int(s) == 0 {
		return errors.New("must set StorageType")
	}
	return nil
}

// VM represents a VM inside of Clockworks
type VM struct {
	ID                int        `json:"id"`
	Name              string     `json:"name,omitempty"`
	GuestFqdn         string     `json:"guest_fqdn,omitempty"`
	MemGB             float64    `json:"mem_gb,omitempty"` // MemGB looks like an int in test, but is in fact a float
	MemMB             int        `json:"mem_mb,omitempty"`
	LocationID        int        `json:"location_id,omitempty"`
	CPU               int        `json:"cpu,omitempty"`
	PatchWindowID     int        `json:"patch_window_id,omitempty"`
	SysadminOptionID  int        `json:"sysadmin_option_id,omitempty"`
	SelfAdmin         bool       `json:"self_admin,omitempty"`
	Folder            string     `json:"folder,omitempty"`
	FundCodes         []FundCode `json:"fund_codes,omitempty"`
	UUID              string     `json:"uuid,omitempty"`
	SerialNumber      string     `json:"serial_number,omitempty"`
	Type              string     `json:"type,omitempty"`
	MoRef             string     `json:"mo_ref,omitempty"`
	MobURL            string     `json:"mob_url,omitempty"`
	WebURL            string     `json:"web_url,omitempty"`
	APIURL            string     `json:"api_url,omitempty"`
	ServiceLevel      string     `json:"service_level,omitempty"`
	StorageType       string     `json:"storage_type,omitempty"`
	ProductionLevel   string     `json:"production_level,omitempty"`
	ToolsStatus       string     `json:"tools_status,omitempty"`
	OSId              string     `json:"os_id,omitempty"`
	OsOptionID        string     `json:"os_option_id,omitempty"`
	CloudInitUserData string     `json:"cloud_init_user_data,omitempty"`
	Disks             []VMDisk   `json:"disks,omitempty"`
	SysadminNetids    []string   `json:"sysadmin_netids,omitempty"`
	AppAdminNetids    []string   `json:"app_admin_netids,omitempty"`
	MacAddresses      []string   `json:"mac_addresses,omitempty"`
	IPAddresses       []string   `json:"ip_addresses,omitempty"`
}

// NewDisk creates a new VMDisk object, with a given size in GB
func NewDisk(s int) VMDisk {
	return VMDisk{
		SizeGB: s,
	}
}

// NewDiskWithID creates a new VMDisk object with the given size and id
func NewDiskWithID(s int, id int) VMDisk {
	return VMDisk{
		DiskID: id,
		SizeGB: s,
	}
}

// NewDiskSet creates a new DiskSet with the given sizes in GB
func NewDiskSet(sizes []int) DiskSet {
	ds := make(DiskSet, len(sizes))
	for idx, s := range sizes {
		ds[idx] = NewDiskWithID(s, idx)
	}
	return ds
}

// NewDiskSetPTR returns a pointer to a new disk set
func NewDiskSetPTR(sizes []int) *DiskSet {
	ds := NewDiskSet(sizes)
	return &ds
}

// DiskSet describes a slice of VMDisks
type DiskSet []VMDisk

// Validate ensures that we have a good DiskSet
func (ds DiskSet) Validate() error {
	if len(ds) == 0 {
		return errors.New("must specify at least 1 disk")
	}
	return nil
}

// VMService contains all the methods needed for interacting with the VM endpoint
type VMService interface {
	// Read only Operations
	List(context.Context, *VMListOpts) ([]VM, *Response, error)
	ListWithLocation(context.Context, int, *LocationListVMOpt) ([]VM, *Response, error)
	Get(context.Context, int) (*VM, *Response, error)
	GetWithFQDN(context.Context, string) (*VM, *Response, error)
	GetIDWithFQDN(context.Context, string, *VMListOpts) (int, *Response, error)

	// Shell Helpers
	ShellComplete(context.Context, string) []string

	// Edit Operations
	Edit(context.Context, int, *VMEditOpts) (*VMRequestResponse, *Response, error)
	Delete(context.Context, int, *VMDeleteOpts) (*VMRequestResponse, *Response, error)
	Create(context.Context, *VMCreateOpts) (*VMRequestResponse, *Response, error)
}

// VMListOpts are the options for listing VMs
type VMListOpts struct {
	Name          string     `url:"name,omitempty"`
	SysadminnedBy string     `url:"sysadminned_by,omitempty"`
	HideDetails   bool       `url:"hide_details"`
	ShowDeleted   bool       `url:"show_deleted"`
	ShowArchived  bool       `url:"show_archived"`
	ShowLive      bool       `url:"show_live"`
	LocationID    LocationID `url:"location_id,omitempty"`
	IPAddress     string     `url:"ip_address,omitempty"`
}

// NewVMListOpts generates a new VMListOpts object
func NewVMListOpts[T VMListOpts](opts ...Option[T]) *T {
	lo := &T{
		HideDetails: true,
		ShowLive:    true,
	}
	for _, o := range opts {
		o(lo)
	}
	return lo
}

// VMDeleteOpts defines options for deleting a VM
type VMDeleteOpts struct {
	ArchiveDeleteTime *time.Time `json:"archive_delete_time,omitempty"`
	StartTime         *time.Time `json:"start_time,omitempty"`
}

// VMEditOpts defines options for editing a VM
type VMEditOpts struct {
	FundCode         FundCode       `json:"fund_code,omitempty"`
	RAM              RAM            `json:"ram,omitempty"`
	CPU              CPU            `json:"cpu,omitempty"`
	Disks            *DiskSet       `json:"disks,omitempty"`
	StorageType      StorageType    `json:"storage_type,omitempty"`
	SysadminOptionID SysadminOption `json:"sysadmin_option_id,omitempty"`
	PoweroffAllowed  bool           `json:"poweroff_allowed"`
	StartTime        *time.Time     `json:"start_time,omitempty"`
}

// VMCreateOpts specifies options for creating a new VM
type VMCreateOpts struct {
	CPU               CPU               `json:"cpu,omitempty"`
	LocationID        LocationID        `json:"location_id,omitempty"`
	PatchWindowID     int               `json:"patch_window_id,omitempty"`
	RAM               RAM               `json:"ram,omitempty"`
	SecureProjectID   int               `json:"secure_project_id,omitempty"`
	SysadminOptionID  SysadminOption    `json:"sysadmin_option_id,omitempty"`
	Backups           bool              `json:"backups,omitempty"`
	DisasterRecovery  bool              `json:"disaster_recovery,omitempty"`
	SkipFinalize      bool              `json:"skip_finalize,omitempty"`
	OSID              OSID              `json:"os_id,omitempty"`
	Hostname          Hostname          `json:"hostname,omitempty"`
	FundCode          FundCode          `json:"fund_code,omitempty"`
	NetworkType       NetworkType       `json:"network_type,omitempty"`
	Subnet            string            `json:"subnet,omitempty"`
	ContainerName     string            `json:"container_name,omitempty"`
	CloudInitUserData CloudInitUserData `json:"cloud_init_user_data,omitempty"`
	ProductionLevel   ProductionLevel   `json:"production_level,omitempty"`
	StorageType       StorageType       `json:"storage_type,omitempty"`
	ApplicationName   string            `json:"application_name,omitempty"`
	Disks             DiskSet           `json:"disks,omitempty"`
	OnBehalfOf        string            `json:"on_behalf_of,omitempty"`
	StartTime         *time.Time        `json:"start_time,omitempty"`
	skipValidate      bool
}

// NewVMDeleteOpts creates a new VMDeleteOpts using functional options
func NewVMDeleteOpts[T VMDeleteOpts](opts ...Option[T]) *T {
	aat := time.Now().Add(time.Hour * 24 * 7)

	do := &T{
		ArchiveDeleteTime: &aat,
	}
	for _, o := range opts {
		o(do)
	}
	return do
}

// NewVMEditOpts uses functional options to return a new VMEditOpts item
func NewVMEditOpts[T VMEditOpts](opts ...Option[T]) *T {
	eo := &T{}
	for _, o := range opts {
		o(eo)
	}
	return eo
}

// NewVMCreateOpts uses functional options to return a new VMCreateOpts item
func NewVMCreateOpts[T VMCreateOpts](opts ...Option[T]) *T {
	co := &T{
		RAM:               1,
		CPU:               1,
		NetworkType:       PrivateNetwork,
		CloudInitUserData: "#cloud-config",
		StorageType:       BronzeStorage,
		Disks:             NewDiskSet([]int{50}),
	}

	for _, o := range opts {
		o(co)
	}

	// Do we need this switch? Check it again when we have the generic fully implemented
	switch x := any(co).(type) {
	case *VMCreateOpts:
		if !x.skipValidate {
			err := x.Validate()
			if err != nil {
				panic(err)
			}
		}
	default:
		panic("unexpected use")
	}
	return co
}

// Hostname  represents a valid hostname for a Clockworks interaction
type Hostname string

// Validate returns an error when the hostname is invalid
func (h Hostname) Validate() error {
	if h == "" {
		return errors.New("hostname cannot be blank")
	}
	if !strings.Contains(string(h), ".") {
		return errors.New("hostname must contain a . in it")
	}
	return nil
}

// VMDisk contains the information needed for interacting with disks on a VM inside of Clockworks
type VMDisk struct {
	DiskID int `json:"disk_id,omitempty"`
	SizeGB int `json:"size_gb,omitempty"`
}

// Validate makes sure we aren't just passing an empty one in here with no changes
func (v *VMEditOpts) Validate() error {
	if *v == (VMEditOpts{}) {
		return errors.New("no changes in VMEditOpts. Be sure to add a non-empty value to at least one of the fields")
	}

	return nil
}

// VMServiceOp is the operator for the VMService
type VMServiceOp struct {
	client *Client
}

// ShellComplete provides shell completion for VM stuff
func (s *VMServiceOp) ShellComplete(ctx context.Context, filter string) []string {
	items, _, err := s.List(ctx, NewVMListOpts(WithName(filter)))
	if err != nil {
		panic(err)
		// return nil
	}
	var ret []string
	for _, i := range items {
		if strings.Contains(i.Name, filter) {
			ret = append(ret, fmt.Sprintf("%v\t%v", i.Name, i.Folder))
		}
	}
	return ret
}

// List will return all VMs matching the passed options
func (s *VMServiceOp) List(ctx context.Context, opt *VMListOpts) ([]VM, *Response, error) {
	if opt == nil {
		opt = NewVMListOpts()
	}
	path, err := addOptions(vmPath, opt)
	if err != nil {
		return nil, nil, err
	}

	req := s.client.mustNewRequest(http.MethodGet, path, nil)

	// root := new(accountRoot)
	var items []VM
	resp, err := s.client.Do(ctx, req, &items)
	if err != nil {
		return nil, resp, err
	}

	return items, resp, err
}

// Get returns a single VM from the ID
func (s *VMServiceOp) Get(ctx context.Context, name int) (*VM, *Response, error) {
	path := fmt.Sprintf("%v/%v", vmPath, name)

	req := s.client.mustNewRequest(http.MethodGet, path, nil)

	// root := new(accountRoot)
	var item VM
	resp, err := s.client.Do(ctx, req, &item)
	if err != nil {
		return nil, resp, err
	}

	return &item, resp, err
}

// GetWithFQDN returns a VM after being given the FQDN
func (s *VMServiceOp) GetWithFQDN(ctx context.Context, fqdn string) (*VM, *Response, error) {
	vmid, resp, err := s.GetIDWithFQDN(ctx, fqdn, nil)
	if err != nil {
		return nil, resp, err
	}

	item, resp, err := s.Get(ctx, vmid)
	if err != nil {
		return nil, resp, err
	}

	return item, resp, err
}

// ListWithLocation returns a list of VMs given the location id and list options
func (s *VMServiceOp) ListWithLocation(ctx context.Context, id int, c *LocationListVMOpt) ([]VM, *Response, error) {
	// If we are given a nil option object, use some sane defaults
	if c == nil {
		c = &LocationListVMOpt{
			ShowDeleted:  false,
			ShowArchived: false,
			ShowLive:     true,
		}
	}
	path := fmt.Sprintf("api/v1/locations/%v/vms", id)
	path, err := addOptions(path, c)
	if err != nil {
		return nil, nil, err
	}

	req := s.client.mustNewRequest(http.MethodGet, path, nil)

	// root := new(accountRoot)
	var target []VM
	resp, err := s.client.Do(ctx, req, &target)
	if err != nil {
		return nil, resp, err
	}

	return target, resp, err
}

// GetIDWithFQDN Given the FQDN of a host, return the clockworks ID.
func (s *VMServiceOp) GetIDWithFQDN(ctx context.Context, fqdn string, opt *VMListOpts) (int, *Response, error) {
	if opt == nil {
		opt = NewVMListOpts()
	}
	opt.Name = fqdn
	path, err := addOptions(vmPath, opt)
	if err != nil {
		return 0, nil, err
	}

	req := s.client.mustNewRequest(http.MethodGet, path, nil)

	// root := new(accountRoot)
	var items []VM
	resp, err := s.client.Do(ctx, req, &items)
	if err != nil {
		return 0, resp, err
	}
	possibleIDs := []int{}
	for _, item := range items {
		if item.Name == fqdn {
			possibleIDs = append(possibleIDs, item.ID)
		}
	}
	if len(possibleIDs) == 0 {
		return 0, resp, fmt.Errorf("no VM with fqdn %v found", fqdn)
	}
	if len(possibleIDs) > 1 {
		return 0, resp, fmt.Errorf("more than one VM with the name %s", fqdn)
	}
	return possibleIDs[0], resp, err
}

// Edit allows modification to a VM given the ID and VMEditOpts
func (s *VMServiceOp) Edit(ctx context.Context, vmid int, opt *VMEditOpts) (*VMRequestResponse, *Response, error) {
	if opt == nil {
		return nil, nil, errors.New("VMEditOpts cannot be nil")
	}
	path := fmt.Sprintf("%v/%v", vmPath, vmid)

	req := s.client.mustNewRequest(http.MethodPut, path, opt)

	var item VMRequestResponse
	resp, err := s.client.Do(ctx, req, &item)
	if err != nil {
		return nil, resp, err
	}
	return &item, resp, nil
}

// Delete deletes a VM
func (s *VMServiceOp) Delete(ctx context.Context, vmid int, opt *VMDeleteOpts) (*VMRequestResponse, *Response, error) {
	if opt == nil {
		opt = &VMDeleteOpts{}
	}
	path := fmt.Sprintf("%v/%v", vmPath, vmid)

	req := s.client.mustNewRequest(http.MethodDelete, path, opt)

	var item VMRequestResponse
	resp, err := s.client.Do(ctx, req, &item)
	if err != nil {
		return nil, resp, err
	}
	return &item, resp, nil
}

// Create will create a VM with the given VMCreateOpts
func (s *VMServiceOp) Create(ctx context.Context, opt *VMCreateOpts) (*VMRequestResponse, *Response, error) {
	if opt == nil {
		return nil, nil, errors.New("must not use a nil VMCreateOpts")
	}

	err := opt.Validate()
	if err != nil {
		return nil, nil, err
	}
	req := s.client.mustNewRequest(http.MethodPost, vmPath, opt)

	var item VMRequestResponse
	resp, err := s.client.Do(ctx, req, &item)
	if err != nil {
		return nil, resp, err
	}
	return &item, resp, nil
}

// Validate Loops through various validators and makes sure a VMCreate request is ok
// Wondering if we should move this to the VMCreateOpts object instead of having a public method for this - DS
func (opt VMCreateOpts) Validate() error {
	// Single use validators. These can validate themselves on their own
	itemValidators := []Validator{
		opt.Hostname,
		opt.Disks,
		opt.StorageType,
		opt.FundCode,
		opt.CloudInitUserData,
		opt.CPU,
		opt.RAM,
		opt.OSID,
		opt.LocationID,
		opt.SysadminOptionID,
	}
	for _, iv := range itemValidators {
		err := iv.Validate()
		if err != nil {
			return err
		}
	}

	// Make sure we have a container if needed
	if err := NewContainer(opt.ContainerName).ValidateWithSysadminOption(opt.SysadminOptionID); err != nil {
		return err
	}

	// Make sure Network stuff is cool
	if err := opt.NetworkType.ValidateWithSubnet(opt.Subnet); err != nil {
		return err
	}

	// Patchwindow stuff
	if err := ValidatePatchWindowWithSysadminOption(opt.PatchWindowID, opt.SysadminOptionID); err != nil {
		return err
	}

	return nil
}
