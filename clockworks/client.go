/*
Package clockworks interacts with the Clockworks API service:

	https://clockworks.oit.duke.edu/help/api_v1
*/
package clockworks

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"net"
	"net/http"
	"net/url"
	"os"
	"reflect"
	"time"

	"github.com/google/go-querystring/query"
	"github.com/spf13/viper"
	"moul.io/http2curl"
)

const (
	libraryVersion = "v1"
	defaultBaseURL = "https://clockworks.oit.duke.edu"
	userAgent      = "clockworks-sdk/" + libraryVersion
	mediaType      = "application/json"
)

// Response is just a holder for the response and any metadata we wanna keep
type Response struct {
	*http.Response
}

// ErrorResponse holds the http response, and any additional metadata when an error is returned
type ErrorResponse struct {
	// HTTP response that caused this error
	Response *http.Response

	// Error message
	Message string   `json:"message"`
	Errors  []string `json:"errors"`
}

// Error turns the response in to an Error
func (r *ErrorResponse) Error() string {
	return fmt.Sprintf("%v %v: %d %v",
		r.Response.Request.Method, r.Response.Request.URL, r.Response.StatusCode, r.Message)
}

// Client is the holder for all the services and methods for interacting with the API
type Client struct {
	// HTTP client used to communicate with the DO API.
	client *http.Client

	// Base URL for API requests.
	BaseURL *url.URL
	// baseURLString string
	Username string
	Token    string

	// User agent for client
	UserAgent string

	// waitTimes is a table with how long to wait for each type of request
	reqWaitTimes map[string]time.Duration

	// Optional extra HTTP headers to set on every request to the API.
	headers map[string]string

	// Error Writer to be used when outputting (perhaps?) useful data.
	// Unsure if this should actually be a logging interface, but keeping it
	// simple for now
	errWriter io.Writer

	// if set, we'll dump out CURL commands on every request
	curlDebug bool

	Location    LocationService
	PatchWindow PatchWindowService
	Container   ContainerService
	OS          OSService
	Network     NetworkService
	VMRequest   VMRequestService
	VM          VMService
	Logger      *slog.Logger
}

// withFakeCredentials just sets the username and token to none empty strings.
// This should only be used for testing
func withFakeCredentials() func(*Client) {
	return func(c *Client) {
		c.Token = "this-is-a-fake-token"
		c.Username = "this-is-a-fake-username"
	}
}

// WithEnv gets Username, Token and URL from environment variables
func WithEnv() func(*Client) {
	return func(c *Client) {
		c.Username = envOrPanic("CLOCKWORKS_USERNAME", "CLOCKWORKS_USERNAME is required")
		c.Token = envOrPanic("CLOCKWORKS_TOKEN", "CLOCKWORKS_TOKEN is required")
		c.BaseURL = mustParseURL(envWithDefault("CLOCKWORKS_BASE_URL", defaultBaseURL))
	}
}

// WithViper gets Username, Token and URL from Viper
func WithViper(v *viper.Viper) func(*Client) {
	var ul *url.URL
	if us := v.GetString("base_url"); us != "" {
		ul = mustParseURL(us)
	} else {
		ul = mustParseURL(defaultBaseURL)
	}
	return func(c *Client) {
		c.Username = viperOrPanic("username", "username (CLOCKWORKS_USERNAME) is required", v)
		c.Token = viperOrPanic("token", "token (CLOCKWORKS_TOKEN) is required", v)
		c.BaseURL = ul
	}
}

// WithUser passes the username in to a new client
func WithUser(u string) func(*Client) {
	return func(c *Client) {
		c.Username = u
	}
}

// WithToken passes the token in to a new client
func WithToken(t string) func(*Client) {
	return func(c *Client) {
		c.Token = t
	}
}

// WithURL passes the base url in to a new client
func WithURL(u string) func(*Client) {
	return func(c *Client) {
		c.BaseURL = mustParseURL(u)
	}
}

// WithReqWaitTimes overrides the default timing when waiting for a request to complete
func WithReqWaitTimes(ts map[string]time.Duration) func(*Client) {
	return func(c *Client) {
		c.reqWaitTimes = ts
	}
}

// WithErrWriter sets the error writer to something custom
func WithErrWriter(w io.Writer) func(*Client) {
	return func(c *Client) {
		c.errWriter = w
	}
}

// WithLogger sets the Logger on a new Client.
func WithLogger(l *slog.Logger) func(*Client) {
	return func(c *Client) {
		c.Logger = l
	}
}

// New uses functional options to create a new client
func New(opts ...func(*Client)) *Client {
	c := &Client{
		client:    http.DefaultClient,
		BaseURL:   mustParseURL(defaultBaseURL),
		UserAgent: userAgent,
		errWriter: os.Stderr,
		reqWaitTimes: map[string]time.Duration{
			"servicenow": time.Second * 30,
			"pending":    time.Second * 30,
			"running":    time.Second * 15,
			"snfinalize": time.Second * 1,
		},
		Logger: slog.New(slog.NewTextHandler(os.Stderr, nil)),
	}
	for _, o := range opts {
		o(c)
	}
	// Set CURL PRINT if it's in the env
	if os.Getenv("CLOCKWORKS_CURL_PRINT") != "" {
		c.curlDebug = true
	}

	// Set up the base stuff
	c.headers = make(map[string]string)
	c.Location = &LocationServiceOp{client: c}
	c.PatchWindow = &PatchWindowServiceOp{client: c}
	c.Container = &ContainerServiceOp{client: c}
	c.OS = &OSServiceOp{client: c}
	c.Network = &NetworkServiceOp{client: c}
	c.VMRequest = &VMRequestServiceOp{client: c}
	c.VM = &VMServiceOp{client: c}

	err := c.Validate()
	if err != nil {
		panic(err)
	}

	return c
}

// Validate ensures that enough pieces of the client are set to use (Username and Token)
func (c *Client) Validate() error {
	switch {
	case c.Token == "":
		return errors.New("must set a token")
	case c.Username == "":
		return errors.New("must set a user")
	default:
		return nil
	}
}

// NewFromEnvironment is just a shortcut for New(WithEnv())
// Keeping it around for backwards compatibility (for now...)
// Deprecated: Use New(WithEnv()) instead going forward
func NewFromEnvironment() (c *Client, err error) {
	defer func() {
		if recover() != nil {
			err = errors.New("could not create client from env. Please ensure the following are set: [CLOCKWORKS_USERNAME,CLOCKWORKS_TOKEN]")
		}
	}()
	c = New(WithEnv())
	return c, nil
}

// mustNewRequest panics if the NewRequest returns an error
func (c *Client) mustNewRequest(method, urlStr string, body interface{}) *http.Request {
	req, err := c.newRequest(method, urlStr, body)
	if err != nil {
		panic(err)
	}
	return req
}

// newRequest builds a new http.Request using some assumptions built in to our client
// Do we really need this? Wondering if we should throw it away in favor of a normal 'http.NewRequest' - DS
func (c *Client) newRequest(method, urlStr string, body interface{}) (*http.Request, error) {
	u, err := c.BaseURL.Parse(urlStr)
	if err != nil {
		return nil, err
	}

	var req *http.Request
	switch method {
	case http.MethodGet, http.MethodHead, http.MethodOptions:
		req = mustNewRequest(method, u.String(), nil)

	default:
		buf := new(bytes.Buffer)
		if body != nil {
			err = json.NewEncoder(buf).Encode(body)
			if err != nil {
				return nil, err
			}
		}

		req = mustNewRequest(method, u.String(), buf)
		req.Header.Set("Content-Type", mediaType)
	}

	for k, v := range c.headers {
		req.Header.Add(k, v)
	}

	req.Header.Set("Accept", mediaType)
	req.Header.Set("User-Agent", c.UserAgent)

	req.SetBasicAuth(c.Username, c.Token)

	if c.curlDebug {
		command, _ := http2curl.GetCurlCommand(req)
		c.Logger.Info("curl command", "cmd", command)
	}
	return req, nil
}

// Do executes the http request
func (c *Client) Do(ctx context.Context, req *http.Request, v interface{}) (*Response, error) {
	resp, err := DoRequestWithClient(ctx, c.client, req)
	if err != nil {
		return nil, err
	}

	defer func() {
		// Ensure the response body is fully read and closed
		// before we reconnect, so that we reuse the same TCPConnection.
		// Close the previous response's body. But read at least some of
		// the body so if it's small the underlying TCP connection will be
		// re-used. No need to check for errors: if it fails, the Transport
		// won't reuse it anyway.
		const maxBodySlurpSize = 2 << 10
		if resp.ContentLength == -1 || resp.ContentLength <= maxBodySlurpSize {
			// This seems to always error out, so just ignore it for now
			_, _ = io.CopyN(io.Discard, resp.Body, maxBodySlurpSize)
		}

		if rerr := resp.Body.Close(); err == nil {
			err = rerr
		}
	}()

	response := newResponse(resp)

	err = c.CheckResponse(resp)
	if err != nil {
		return response, err
	}

	if v != nil {
		if w, ok := v.(io.Writer); ok {
			_, err = io.Copy(w, resp.Body)
			if err != nil {
				return nil, err
			}
		} else {
			err = json.NewDecoder(resp.Body).Decode(v)
			if err != nil {
				return nil, err
			}
		}
	}

	return response, err
}

func newResponse(r *http.Response) *Response {
	response := Response{Response: r}

	return &response
}

// CheckResponse examines an http.Response and returns an error if it's not cool 😎
func (c *Client) CheckResponse(r *http.Response) error {
	if cd := r.StatusCode; cd >= 200 && cd <= 299 {
		return nil
	}

	errorResponse := &ErrorResponse{Response: r}
	data, err := io.ReadAll(r.Body)
	if err == nil && len(data) > 0 {
		err := json.Unmarshal(data, errorResponse)
		if err != nil {
			errorResponse.Message = string(data)
		}
		for _, e := range errorResponse.Errors {
			c.Logger.Warn("error returned from clockworks", "error", e, "code", errorResponse.Response.StatusCode)
		}
	}

	return errorResponse
}

// DoRequest executes a request with the default http client
// Do we really need this one? - DS
func DoRequest(ctx context.Context, req *http.Request) (*http.Response, error) {
	return DoRequestWithClient(ctx, http.DefaultClient, req)
}

// DoRequestWithClient submits an HTTP request using the specified client.
func DoRequestWithClient(ctx context.Context, client *http.Client, req *http.Request) (*http.Response, error) {
	req = req.WithContext(ctx)
	return client.Do(req)
}

func addOptions(s string, opt interface{}) (string, error) {
	v := reflect.ValueOf(opt)

	if v.Kind() == reflect.Ptr && v.IsNil() {
		return s, nil
	}

	origURL, err := url.Parse(s)
	if err != nil {
		return s, err
	}

	origValues := origURL.Query()

	newValues, err := query.Values(opt)
	if err != nil {
		return s, err
	}

	for k, v := range newValues {
		origValues[k] = v
	}

	origURL.RawQuery = origValues.Encode()
	return origURL.String(), nil
}

// GuessPrimaryNetworkWithVMId returns a network that is guessed using the VM ID
func (c *Client) GuessPrimaryNetworkWithVMId(ctx context.Context, id int) (*Network, error) {
	vm, _, err := c.VM.Get(ctx, id)
	if err != nil {
		return nil, err
	}
	possibleNetworks, _, err := c.Network.ListWithLocation(ctx, vm.LocationID)
	if err != nil {
		return nil, err
	}

	for _, ip := range vm.IPAddresses {
		for _, possibleNet := range possibleNetworks {
			_, netC, _ := net.ParseCIDR(possibleNet.Subnet)
			ipC := net.ParseIP(ip)
			if netC.Contains(ipC) {
				return &possibleNet, nil
			}
		}
	}
	return nil, errors.New("could not find a network for this VM")
}

// GuessPrimaryNetworkWithFQDN attempts to find the primary network based off of the FQDN
func (c *Client) GuessPrimaryNetworkWithFQDN(ctx context.Context, fqdn string) (*Network, error) {
	vm, _, err := c.VM.GetWithFQDN(ctx, fqdn)
	if err != nil {
		return nil, err
	}
	network, err := c.GuessPrimaryNetworkWithVMId(ctx, vm.ID)
	if err != nil {
		return nil, err
	}
	return network, nil
}
