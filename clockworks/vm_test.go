package clockworks

import (
	"fmt"
	"net/http"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestListVMs(t *testing.T) {
	// Create a new client
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/vms", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)

		content, _ := os.ReadFile("testdata/vms.json")
		fmt.Fprint(w, string(content))
	})

	items, _, err := client.VM.List(ctx, nil)
	require.NoError(t, err)
	require.IsType(t, []VM{}, items)
	require.Greater(t, len(items), 0)
}

func TestVMShellComplete(t *testing.T) {
	// Create a new client
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/vms", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)

		content, _ := os.ReadFile("testdata/vms.json")
		fmt.Fprint(w, string(content))
	})

	got := client.VM.ShellComplete(ctx, "")
	require.IsType(t, []string{}, got)
	require.Greater(t, len(got), 1)
	require.Equal(t, "acme-bigip-02.example.com\tSelf-Admin/FOO-SSI-EIS", got[0])
	// require.Equal(t, "foo", got)
}

func TestGetVM(t *testing.T) {
	// Create a new client
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/vms/1", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)

		content, _ := os.ReadFile("testdata/vm.json")
		fmt.Fprint(w, string(content))
	})

	items, _, err := client.VM.Get(ctx, 1)
	require.NoError(t, err)
	require.IsType(t, &VM{}, items)
	require.Equal(t, FundCode("012-3456"), items.FundCodes[0])
	require.Equal(t, 1, items.SysadminOptionID)
	require.Equal(t, 1, items.LocationID)
}

func TestGetLocationVMs(t *testing.T) {
	// Create a new client
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/locations/1/vms", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)
		content, _ := os.ReadFile("testdata/locations/1-vms.json")
		fmt.Fprint(w, string(content))
	})

	items, _, err := client.VM.ListWithLocation(ctx, 1, nil)
	require.NoError(t, err)
	require.NotNil(t, items)
}

func TestGetIDWithFQDN(t *testing.T) {
	// Create a new client
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/vms", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)
		content, _ := os.ReadFile("testdata/similar-vm-names.json")
		fmt.Fprint(w, string(content))
	})

	item, _, err := client.VM.GetIDWithFQDN(ctx, "acme-01.example.com", nil)
	require.NoError(t, err)
	require.NotNil(t, item)
	require.Equal(t, 2, item)

	_, _, err = client.VM.GetIDWithFQDN(ctx, "acme-01.never-exist.com", nil)
	require.Error(t, err)
}

func TestGetWithFQDN(t *testing.T) {
	// Create a new client
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/vms/484", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)
		content := `{"id": 484, "name": "acme-bigip-02.example.com"}`
		fmt.Fprint(w, content)
	})

	mux.HandleFunc("/api/v1/vms", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)
		content, _ := os.ReadFile("testdata/vms.json")
		fmt.Fprint(w, string(content))
	})

	item, _, err := client.VM.GetWithFQDN(ctx, "acme-bigip-02.example.com")
	require.NoError(t, err)
	require.NotNil(t, item)
	require.IsType(t, &VM{}, item)
}

func TestEditVM(t *testing.T) {
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/vms/1", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodPut)
		content := `{"sn_request_id": "TASK1234"}`
		fmt.Fprint(w, content)
	})

	opt := VMEditOpts{
		RAM: 2048,
	}

	item, _, err := client.VM.Edit(ctx, 1, &opt)
	require.NoError(t, err)
	require.NotNil(t, item)
	require.IsType(t, &VMRequestResponse{}, item)
}

func TestDeleteVM(t *testing.T) {
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/vms/1", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodDelete)
		content := `{"sn_request_id": "TASK9876"}`
		fmt.Fprint(w, content)
	})

	item, _, err := client.VM.Delete(ctx, 1, nil)
	require.NoError(t, err)
	require.NotNil(t, item)
	require.IsType(t, &VMRequestResponse{}, item)
}

func TestVMEditOptsValidate(t *testing.T) {
	ts := []struct {
		opt     VMEditOpts
		wantErr bool
	}{
		{
			opt:     VMEditOpts{},
			wantErr: true,
		},
		{
			opt: VMEditOpts{
				RAM: 10240,
			},
			wantErr: false,
		},
		{
			opt: VMEditOpts{
				FundCode:         "000-1234",
				RAM:              10240,
				CPU:              4,
				StorageType:      BronzeStorage,
				SysadminOptionID: 5,
				Disks:            NewDiskSetPTR([]int{50}),
			},
			wantErr: false,
		},
	}
	for _, tt := range ts {
		err := tt.opt.Validate()
		if tt.wantErr {
			require.Error(t, err)
		} else {
			require.NoError(t, err)
		}
	}
}

func TestCreateVM(t *testing.T) {
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/vms", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodPost)
		content := `{"sn_request_id": "TASK4321"}`
		fmt.Fprint(w, content)
	})
	mux.HandleFunc("/api/v1/locations", func(w http.ResponseWriter, _ *http.Request) {
		content := `[{"id": 1, "name": "ACME-Building"}]`
		fmt.Fprint(w, content)
	})
	mux.HandleFunc("/api/v1/oses", func(w http.ResponseWriter, _ *http.Request) {
		content := `[{"id": 1, "os_id": "ubuntu18"}]`
		fmt.Fprint(w, content)
	})
	mux.HandleFunc("/api/v1/containers", func(w http.ResponseWriter, _ *http.Request) {
		content := `[{"name": "foo"}]`
		fmt.Fprint(w, content)
	})
	mux.HandleFunc("/api/v1/patch_windows", func(w http.ResponseWriter, _ *http.Request) {
		content := `[{"id": -1}, {"id": 1}]`
		fmt.Fprint(w, content)
	})

	opt := VMCreateOpts{
		LocationID:        1,
		Hostname:          "foo.bar.com",
		OSID:              "ubuntu18",
		FundCode:          "000-1234",
		NetworkType:       PrivateNetwork,
		ContainerName:     "foo",
		CloudInitUserData: "Hello",
		SysadminOptionID:  1,
		RAM:               1,
		CPU:               1,
		StorageType:       BronzeStorage,
		Disks: []VMDisk{
			{SizeGB: 50},
		},
	}

	err := opt.Validate()
	require.NoError(t, err)

	item, _, err := client.VM.Create(ctx, &opt)
	require.NoError(t, err)
	require.NotNil(t, item)
	require.IsType(t, &VMRequestResponse{}, item)
}

func TestValidateHostname(t *testing.T) {
	setup()
	defer teardown()
	ts := []struct {
		hostname Hostname
		wantErr  bool
	}{
		{hostname: "", wantErr: true},
		{hostname: "foo", wantErr: true},
		{hostname: "foo.com", wantErr: false},
	}

	for _, tt := range ts {
		if tt.wantErr {
			require.Error(t, tt.hostname.Validate())
		} else {
			require.NoError(t, tt.hostname.Validate())
		}
	}
}

/*
func TestValidateProductionLevel(t *testing.T) {
	setup()
	defer teardown()
	ts := []struct {
		level   string
		wantErr bool
	}{
		{"", true},
		{"foo", true},
		{"production", false},
	}

	for _, tt := range ts {
		err := client.VM.ValidateProductionLevel(ctx, tt.level)
		if tt.wantErr {
			require.Error(t, err)
		} else {
			require.NoError(t, err)
		}
	}
}
*/

func TestValidateStorageType(t *testing.T) {
	setup()
	defer teardown()
	ts := []struct {
		level   StorageType
		wantErr bool
	}{
		{level: NullStorageType, wantErr: true},
		{level: PlatinumStorage, wantErr: false},
	}

	for _, tt := range ts {
		if tt.wantErr {
			require.Error(t, tt.level.Validate())
		} else {
			require.NoError(t, tt.level.Validate())
		}
	}
}

func TestStorageType(t *testing.T) {
	tests := []struct {
		nt     StorageType
		expect string
	}{
		{
			nt:     NullStorageType,
			expect: "",
		},
		{
			nt:     PlatinumStorage,
			expect: "platinum",
		},
		{
			nt:     SilverStorage,
			expect: "silver",
		},
		{
			nt:     BronzeStorage,
			expect: "bronze",
		},
	}

	for _, tt := range tests {
		require.Equal(t, tt.expect, tt.nt.String())
	}
}

func TestProductionLevel(t *testing.T) {
	tests := []struct {
		nt     ProductionLevel
		expect string
	}{
		{nt: NullProductionLevel, expect: ""},
		{nt: Production, expect: "production"},
		{nt: Development, expect: "development"},
		{nt: Testing, expect: "testing"},
	}

	for _, tt := range tests {
		require.Equal(t, tt.expect, tt.nt.String())
	}
}

func TestNewCreateVMOpts(t *testing.T) {
	o := NewVMCreateOpts(
		WithHostname("example.com"),
		WithFundCode[VMCreateOpts]("BUNK"),
		WithOSID("some-os"),
		withLocationID[VMCreateOpts](5),
		WithSysadminOption[VMCreateOpts](WorkHoursGeneralSupport),
	)
	require.NotNil(t, o)

	// Make sure defaults were applied
	require.EqualValues(t, 1, o.RAM)
	require.EqualValues(t, 1, o.CPU)
	require.Equal(t, PrivateNetwork, o.NetworkType)
	require.Equal(t, BronzeStorage, o.StorageType)
	require.EqualValues(t, "#cloud-config", o.CloudInitUserData)
}

func TestNewCreateVMOptsEmpty(t *testing.T) {
	// An empty request should always panic as we need some base bits input
	require.Panics(t, func() { NewVMCreateOpts() })
}

func TestNewVMEditOpts(t *testing.T) {
	require.NotNil(t, NewVMEditOpts())
}

func TestStorageTypeWithString(t *testing.T) {
	tests := map[string]struct {
		s         string
		w         StorageType
		wantPanic string
	}{
		"platinum": {
			s: "platinum",
			w: PlatinumStorage,
		},
		"bronze": {
			s: "bronze",
			w: BronzeStorage,
		},
		"silver": {
			s: "silver",
			w: SilverStorage,
		},
		"unknown-storage": {
			s:         "Never-exists",
			wantPanic: "unknown storage type: Never-exists",
		},
	}

	for desc, tt := range tests {
		tt := tt
		if tt.wantPanic != "" {
			require.PanicsWithValue(t, tt.wantPanic, func() { StorageTypeWithString(tt.s) }, desc)
		} else {
			require.Equal(t, tt.w, StorageTypeWithString(tt.s), desc)
		}
	}
}

func TestSysadminOptionSet(t *testing.T) {
	var s SysadminOption
	err := s.Set("self-admin")
	require.NoError(t, err)
	require.Equal(t, SelfAdminSupport, s)

	err = s.Set("non-existing")
	require.Error(t, err)
}
