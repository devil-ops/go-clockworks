package clockworks

import (
	"context"
	"errors"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/spf13/cobra"
)

/*
This file contains methods for interacting with Cobra
*/

var (
	createSysadminOptionID SysadminOption
	createStorageType      = BronzeStorage
	createProductionLevel  = Production
	modifySysadminOptionID = NullSupport
	modifyStorageType      = NullStorageType
)

// Should we have more than just this single help constant? Feels right for more complex help stuff...
const startAtHelp string = `Wait until the given time to start the modification. If no timezone is given,
this will use your local timezone. Format can be in any of the supported formats
from here: https://github.com/araddon/dateparse#extended-example`

// VMDeleteOptsWithCmd returns a delete option with the given options
func (c *Client) VMDeleteOptsWithCmd(cmd *cobra.Command, _ []string) (*VMDeleteOpts, error) {
	opts := []Option[VMDeleteOpts]{}
	if v, ok := mustGetCmdChanged[string](cmd, "start-at"); ok {
		opts = append(opts, WithStartTime[VMDeleteOpts](v))
	}
	daa, err := cmd.Flags().GetDuration("delete-archive-after")
	if err != nil {
		return nil, err
	}
	opts = append(opts, WithDeleteArchiveAfter(daa))
	/*
		if v, ok := mustGetCmdChanged[time.Duration](cmd, "delete-archive-after"); ok {
			opts = append(opts, WithDeleteArchiveAfter(v))
		}
	*/
	opt := NewVMDeleteOpts(opts...)

	return opt, nil
}

// VMEditOptsWithCmd returns an edit option with the given options
func (c *Client) VMEditOptsWithCmd(cmd *cobra.Command, _ []string) (*VMEditOpts, error) {
	opts := []Option[VMEditOpts]{}
	if v, ok := mustGetCmdChanged[int](cmd, "ram"); ok {
		opts = append(opts, WithRAM[VMEditOpts](v))
	}
	if v, ok := mustGetCmdChanged[int](cmd, "cpu"); ok {
		opts = append(opts, WithCPU[VMEditOpts](v))
	}
	if v, ok := mustGetCmdChanged[string](cmd, "fund-code"); ok {
		opts = append(opts, WithFundCode[VMEditOpts](v))
	}
	if modifyStorageType != NullStorageType {
		opts = append(opts, WithStorageType[VMEditOpts](modifyStorageType))
	}

	if modifySysadminOptionID != NullSupport {
		opts = append(opts, WithSysadminOption[VMEditOpts](modifySysadminOptionID))
	}

	if _, ok := mustGetCmdChanged[bool](cmd, "poweroff-allowed"); ok {
		opts = append(opts, WithPoweroffAllowed())
	}

	if v, ok := mustGetCmdChanged[string](cmd, "start-at"); ok {
		opts = append(opts, WithStartTime[VMEditOpts](v))
	}

	opt := NewVMEditOpts(opts...)

	if err := opt.Validate(); err != nil {
		return nil, err
	}

	return opt, nil
}

// VMCreateOptsWithCmd creates a new VMCreatOpts with the cobra.Command info given
func (c *Client) VMCreateOptsWithCmd(cmd *cobra.Command, args []string) (*VMCreateOpts, error) {
	// Place holder for all the options
	opts := []Option[VMCreateOpts]{
		WithHostname(args[0]),
		WithOSID(mustGetCmd[string](cmd, "os")),
		WithDisks[VMCreateOpts](NewDiskSet(mustGetCmd[[]int](cmd, "disk"))),
		// WithStorageType[VMCreateOpts](StorageTypeWithString(mustGetCmd[string](cmd, "storage-type"))),
		WithStorageType[VMCreateOpts](createStorageType),
		WithContainer(mustGetCmd[string](cmd, "container")),
		WithNetworkType(NetworkTypeWithString(mustGetCmd[string](cmd, "network-type"))),
		WithSubnet(mustGetCmd[string](cmd, "subnet")),
		WithRAM[VMCreateOpts](mustGetCmd[int](cmd, "ram")),
		WithCPU[VMCreateOpts](mustGetCmd[int](cmd, "cpu")),
		WithFundCode[VMCreateOpts](mustGetCmd[string](cmd, "fund-code")),
		WithPatchWindowID(mustGetCmd[int](cmd, "patch-window-id")),
		WithApplication(mustGetCmd[string](cmd, "application-name")),
		WithProductionLevel(createProductionLevel),
		WithSecureProjectID(mustGetCmd[int](cmd, "secure-project-id")),
		WithOnBehalfOf(mustGetCmd[string](cmd, "on-behalf-of")),
		WithSysadminOption[VMCreateOpts](createSysadminOptionID),
	}

	// Figure out the location id using the name provided on the CLI
	location, _, err := c.Location.GetWithName(context.TODO(), mustGetCmd[string](cmd, "location"))
	if err != nil {
		return nil, fmt.Errorf("location not found: %v", location)
	}
	opts = append(opts, WithLocation[VMCreateOpts](*location))

	// Cloud Init needs a lil extra
	ci, err := fcOrString(mustGetCmd[string](cmd, "cloud-init"))
	if err != nil {
		return nil, err
	}
	opts = append(opts, WithCloudInitUserData(ci))

	if mustGetCmd[bool](cmd, "skip-finalize") {
		opts = append(opts, WithSkipFinalize())
	}

	// backups and disaster-recovery are a little touchy. They are highly
	// dependant on the location. We attempt to auto-populate the
	// appropriate pieces using the min_allowed_backups, but be careful 🐉
	if mustGetCmd[bool](cmd, "backups") {
		opts = append(opts, WithBackups())
	}
	if _, ok := mustGetCmdChanged[bool](cmd, "disaster-recovery"); ok {
		opts = append(opts, WithDisasterRecovery())
	}

	if v, ok := mustGetCmdChanged[string](cmd, "start-at"); ok {
		opts = append(opts, WithStartTime[VMCreateOpts](v))
	}

	opt := NewVMCreateOpts(opts...)

	return opt, nil
}

// fcOrString returns itself, unless prefixed with a @, then it returns the contents of the file after @
// Example: @/tmp/file.txt
func fcOrString(s string) (string, error) {
	if strings.HasPrefix(s, "@") {
		if len(s) < 2 {
			return "", errors.New("must specify a file after the @")
		}
		var b []byte
		var err error
		b, err = os.ReadFile(s[1:])
		if err != nil {
			return "", err
		}
		return string(b), nil
	}
	return s, nil
}

// mustGetCmdChanged returns the value of a given flag, if it is set. Returns value, ok
func mustGetCmdChanged[T []int | int | string | bool | time.Duration](cmd *cobra.Command, s string) (T, bool) {
	return mustGetCmd[T](cmd, s), cmd.Flags().Changed(s)
}

// mustMarkRequired marks a flag as required, or panics
func mustMarkRequired(cmd *cobra.Command, s string) {
	err := cmd.MarkPersistentFlagRequired(s)
	if err != nil {
		panic(err)
	}
}

// BindDeleteVMCobra creates the required arguments for a delete VM request
func BindDeleteVMCobra(cmd *cobra.Command) error {
	cmd.PersistentFlags().Duration("delete-archive-after", 29*24*time.Hour, "Delete archive after this duration")
	cmd.PersistentFlags().String("start-at", "", startAtHelp)
	return nil
}

// BindEditVMCobra creates the required arguments for a modify VM request
func BindEditVMCobra(cmd *cobra.Command) error {
	cmd.PersistentFlags().Int("ram", 0, "RAM in GB")
	cmd.PersistentFlags().Int("cpu", 0, "CPU Cores")
	cmd.PersistentFlags().String("start-at", "", startAtHelp)
	cmd.PersistentFlags().Var(&modifySysadminOptionID, "sysadmin-option", sysadminOptionHelp())

	if err := cmd.RegisterFlagCompletionFunc("sysadmin-option", SysadminOptionCompletion); err != nil {
		return err
	}

	cmd.PersistentFlags().String("fund-code", "", "Fund Code")
	cmd.PersistentFlags().Var(&modifyStorageType, "storage-type", "Storage Type: (bronze, silver, platinum)")
	if err := cmd.RegisterFlagCompletionFunc("storage-type", StorageTypeCompletion); err != nil {
		return err
	}
	cmd.PersistentFlags().BoolP("poweroff-allowed", "p", false, "Allow poweroff")

	return nil
}

// BindNewVMCobra creates the required arguments for a new VM to the given cobra.Command
func BindNewVMCobra(cmd *cobra.Command, _ *Client) error {
	cmd.PersistentFlags().Int("ram", 1, "RAM in GB")
	cmd.PersistentFlags().Int("cpu", 1, "CPU Cores")
	cmd.PersistentFlags().String("application-name", "", "The CMDB application associated with this VM. Leave blank if unsure")
	cmd.PersistentFlags().Bool("backups", true, "Enable backups for host (Availability depends on location)")
	cmd.PersistentFlags().Bool("disaster-recovery", false, "Enable disaster-recovery backups (Availability depends on location)")
	cmd.PersistentFlags().String("cloud-init", "#cloud-config", "Cloud Init Script. Use @filename to read from a file")
	cmd.PersistentFlags().IntSlice("disk", []int{50}, "Disk Size in GB. Specify multiple times for more disks")
	cmd.PersistentFlags().String("container", "", "Container for Self-Admin VMs")

	// Fund code is required
	cmd.PersistentFlags().String("fund-code", "", "Fund Code for billing 🤑")
	mustMarkRequired(cmd, "fund-code")

	bindLocation(cmd)
	bindOS(cmd)

	cmd.PersistentFlags().String("network-type", "private", "Type of Network for VM (public, private, custom)")
	cmd.PersistentFlags().String("on-behalf-of", "", "Create on behalf of a given NetID")

	cmd.PersistentFlags().Int("patch-window-id", AnyPatchWindow, "ID of the Patchmonkey window (Only valid in SSI Managed VMs)")

	bindProductionLevel(cmd)

	cmd.PersistentFlags().Int("secure-project-id", 0, "ID number of secure project")
	cmd.PersistentFlags().Bool("skip-finalize", false, "Skip finalize ticket step")

	bindStorageType(cmd)
	bindSysadminOption(cmd)

	cmd.PersistentFlags().String("subnet", "", "Subnet to use when network-type is custom")
	cmd.PersistentFlags().String("start-at", "", startAtHelp)

	return nil
}

/*
func addFlag[T string](cmd *cobra.Command, name string, def T, help string) {
	switch any(new(T)).(type) {
	case *string:
		if defa, ok := any(def).(string); ok {
			cmd.PersistentFlags().String(name, defa, help)
		}
	default:
		panic("unexpected usage of addFlag")
	}
}
*/

// BindCreateVMCompletions adds auto completion to the Create VM command
func BindCreateVMCompletions(cmd *cobra.Command, c *Client) error {
	if c != nil {
		bindLocationCompletion(cmd, c)
		bindOSCompletion(cmd, c)
		return nil
	}
	return errors.New("cannot bind completions when client is nil")
}

// bindLocation adds the appropriate pieces to the location flag
func bindLocation(cmd *cobra.Command) {
	cmd.PersistentFlags().String("location", "", "Location Name")
	mustMarkRequired(cmd, "location")
}

func bindLocationCompletion(cmd *cobra.Command, client *Client) {
	if client != nil {
		mustRegisterCompletion(client, cmd, "location", client.Location.ShellComplete)
	} else {
		mustRegisterCompletion(client, cmd, "location", nil)
	}
}

// bindOS adds the appropriate pieces to the OS flag
func bindOS(cmd *cobra.Command) {
	cmd.PersistentFlags().String("os", "", "Operating System")
	mustMarkRequired(cmd, "os")
}

func bindOSCompletion(cmd *cobra.Command, client *Client) {
	if client != nil {
		mustRegisterCompletion(client, cmd, "os", client.OS.ShellComplete)
	} else {
		mustRegisterCompletion(client, cmd, "os", nil)
	}
}

func bindStorageType(cmd *cobra.Command) {
	cmd.PersistentFlags().Var(&createStorageType, "storage-type", "Storage Type: (bronze, silver, platinum)")
	mustMarkRequired(cmd, "storage-type")
	cmd.PersistentFlags().Lookup("storage-type").DefValue = "bronze"
	err := cmd.RegisterFlagCompletionFunc("storage-type", StorageTypeCompletion)
	if err != nil {
		panic(err)
	}
}

func bindSysadminOption(cmd *cobra.Command) {
	cmd.PersistentFlags().Var(&createSysadminOptionID, "sysadmin-option", sysadminOptionHelp())
	mustMarkRequired(cmd, "sysadmin-option")
	err := cmd.RegisterFlagCompletionFunc("sysadmin-option", SysadminOptionCompletion)
	if err != nil {
		panic(err)
	}
}

func bindProductionLevel(cmd *cobra.Command) {
	cmd.PersistentFlags().Var(&createProductionLevel, "production-level", "The production level of a VM (production, development, testing)")
	if err := cmd.RegisterFlagCompletionFunc("production-level", ProductionLevelCompletion); err != nil {
		panic(err)
	}
}
