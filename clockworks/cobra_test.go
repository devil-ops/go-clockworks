package clockworks

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/spf13/cobra"
	"github.com/stretchr/testify/require"
)

// fileWriter is simple wrapper for serving up test content
func fileWriter(w io.Writer, f string) {
	content, err := os.ReadFile(f)
	if err != nil {
		panic(err)
	}
	fmt.Fprint(w, string(content))
}

func TestVMCreateWithCmd(t *testing.T) {
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/locations", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)
		fileWriter(w, "testdata/locations/all.json")
	})

	cmd := &cobra.Command{}
	BindNewVMCobra(cmd, nil)

	cmd.SetArgs([]string{
		"--location", "ATC Lab Dev",
		"--fund-code", "123-4567",
		"--os", "Ubuntu",
		"--sysadmin-option", "8/5",
		// "--storage-type", "bronze",
	})
	err := cmd.Execute()
	require.NoError(t, err)

	opts, err := client.VMCreateOptsWithCmd(cmd, []string{"example.com"})
	require.NoError(t, err)
	require.NotNil(t, opts)
}

func TestFCOrString(t *testing.T) {
	got, err := fcOrString("foo")
	require.NoError(t, err)
	require.Equal(t, "foo", got)

	got, err = fcOrString("@testdata/file.txt")
	require.NoError(t, err)
	require.Equal(t, "This is the contents\n", got)

	got, err = fcOrString("@")
	require.Error(t, err)
	require.EqualError(t, err, "must specify a file after the @")
	require.Equal(t, "", got)
}

func TestVMModifyWithCmd(t *testing.T) {
	setup()
	defer teardown()

	cmd := &cobra.Command{}
	BindEditVMCobra(cmd)

	cmd.SetArgs([]string{
		"--cpu", "5",
		"--fund-code", "123-1234",
		"--ram", "2",
	})
	err := cmd.Execute()
	require.NoError(t, err)

	opts, err := client.VMEditOptsWithCmd(cmd, []string{"example.com"})
	require.NoError(t, err)
	require.NotNil(t, opts)
	require.EqualValues(t, 5, opts.CPU)
	require.EqualValues(t, "123-1234", opts.FundCode)
	require.EqualValues(t, 2, opts.RAM)
}

func TestVMDeleteWithCmd(t *testing.T) {
	setup()
	defer teardown()

	cmd := &cobra.Command{}
	BindDeleteVMCobra(cmd)

	cmd.SetArgs([]string{
		"--start-at", "2025-01-02 13:00",
		"--delete-archive-after", "24h",
	})
	err := cmd.Execute()
	require.NoError(t, err)

	opts, err := client.VMDeleteOptsWithCmd(cmd, []string{"example.com"})
	require.NoError(t, err)
	require.NotNil(t, opts)
	require.EqualValues(t, time.Date(2025, time.January, 2, 13, 0, 0, 0, time.Local), *opts.StartTime)
	require.EqualValues(t, time.Date(2025, time.January, 3, 13, 0, 0, 0, time.Local), *opts.ArchiveDeleteTime)
}
