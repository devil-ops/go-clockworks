package clockworks

import (
	"context"
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestListPatchWindows(t *testing.T) {
	// Create a new client
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/patch_windows", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)
		fileWriter(w, "testdata/patch_windows.json")
	})

	items, _, err := client.PatchWindow.List(ctx)
	require.NoError(t, err)
	require.IsType(t, []PatchWindow{}, items)
	require.Greater(t, len(items), 0)
}

func TestValidatePatchWindow(t *testing.T) {
	setup()
	defer teardown()
	mux.HandleFunc("/api/v1/patch_windows", func(w http.ResponseWriter, _ *http.Request) {
		content := `[{"id": -1}, {"id": 1}]`
		fmt.Fprint(w, content)
	})
	ts := map[string]struct {
		p       int
		s       SysadminOption
		wantErr bool
	}{
		"good": {p: -1, s: SelfAdminSupport, wantErr: true},
		"bad":  {p: -1, s: WorkHoursGeneralSupport, wantErr: false},
	}

	for desc, tt := range ts {
		if tt.wantErr {
			require.Error(t, ValidatePatchWindowWithSysadminOption(tt.p, tt.s), desc)
		} else {
			require.NoError(t, ValidatePatchWindowWithSysadminOption(tt.p, tt.s), desc)
		}
	}
}

func TestPatchWindowShellComplete(t *testing.T) {
	setup()
	defer teardown()
	mux.HandleFunc("/api/v1/patch_windows", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)
		fileWriter(w, "testdata/patch_windows.json")
	})

	got := client.PatchWindow.ShellComplete(context.TODO(), "")
	require.NotNil(t, got)
	require.Greater(t, len(got), 0)
}

func TestPatchWindowListIds(t *testing.T) {
	setup()
	defer teardown()
	mux.HandleFunc("/api/v1/patch_windows", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)
		fileWriter(w, "testdata/patch_windows.json")
	})

	got, _, err := client.PatchWindow.ListIDs(context.TODO())
	require.NoError(t, err)
	require.NotNil(t, got)
	require.Greater(t, len(got), 0)
}
