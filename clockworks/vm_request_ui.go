package clockworks

import (
	"context"
	"fmt"

	"github.com/charmbracelet/bubbles/spinner"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
)

type (
	errMsg error
)

type vmRequestStatusModel struct {
	spinner       spinner.Model
	quitting      bool
	err           error
	client        *Client
	reqid         int
	waitFor       []string
	currentStatus string
}

func vmRequestStatusInitialModel(c *Client, reqid int, statuses []string) vmRequestStatusModel {
	s := spinner.New()
	// s.Spinner = spinner.Dot
	s.Spinner = spinner.Pulse
	s.Style = lipgloss.NewStyle().Foreground(lipgloss.Color("205"))
	return vmRequestStatusModel{
		spinner: s,
		client:  c,
		reqid:   reqid,
		waitFor: statuses,
	}
}

func (m vmRequestStatusModel) Init() tea.Cmd {
	return m.spinner.Tick
}

func (m vmRequestStatusModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "q", "esc", "ctrl+c":
			m.quitting = true
			return m, tea.Quit
		default:
			return m, nil
		}

	case errMsg:
		m.err = msg
		return m, nil

	default:
		// Check status by default
		status, _, err := m.client.VMRequest.Get(context.Background(), m.reqid)
		if err != nil {
			m.err = err
			return m, nil
		}
		m.currentStatus = status.Status
		if containsString(m.waitFor, m.currentStatus) {
			m.quitting = true
			return m, tea.Quit
		}

		// Update the spinner
		var cmd tea.Cmd
		m.spinner, cmd = m.spinner.Update(msg)

		return m, cmd
	}
}

func (m vmRequestStatusModel) View() string {
	if m.err != nil {
		return m.err.Error()
	}
	if m.quitting {
		return "Completed!\n"
	}
	str := fmt.Sprintf("%s Waiting for VMRequest request to complete... | [%v]\n", m.spinner.View(), m.currentStatus)
	str += lipgloss.NewStyle().Foreground(lipgloss.Color("241")).Render(" q - exit wait\n")
	if m.quitting {
		return str + "\n"
	}
	return str
}
