package clockworks

import (
	"fmt"
	"net/http"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestListOSes(t *testing.T) {
	// Create a new client
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/oses", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)

		content, _ := os.ReadFile("testdata/oses.json")
		fmt.Fprint(w, string(content))
	})

	items, _, err := client.OS.List(ctx)
	require.NoError(t, err)
	require.IsType(t, []OS{}, items)
	require.Greater(t, len(items), 0)
}

func TestGetOS(t *testing.T) {
	// Create a new client
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/oses/ubuntu18", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)

		content, _ := os.ReadFile("testdata/os-ubuntu18.json")
		fmt.Fprint(w, string(content))
	})

	items, _, err := client.OS.Get(ctx, "ubuntu18")
	require.NoError(t, err)
	require.IsType(t, &OS{}, items)
}
