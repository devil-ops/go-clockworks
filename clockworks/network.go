package clockworks

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strings"
)

// NetworkType is the base type for a Network configuration option in Clockworks
type NetworkType int

const (
	// NullNetwork is the empty NetworkType
	NullNetwork NetworkType = iota
	// PublicNetwork will use any available public network
	PublicNetwork
	// PrivateNetwork will use any available private network
	PrivateNetwork
	// CustomNetwork will use a given network
	CustomNetwork
)

func (n NetworkType) String() string {
	switch n {
	case PublicNetwork:
		return "public"
	case PrivateNetwork:
		return "private"
	case CustomNetwork:
		return "custom"
	case NullNetwork:
		return ""
	default:
		panic("invalid networktype")
	}
}

// NetworkTypeWithString returns a NetworkType using the given string
func NetworkTypeWithString(s string) NetworkType {
	switch s {
	case "public":
		return PublicNetwork
	case "private":
		return PrivateNetwork
	case "custom":
		return CustomNetwork
	case "":
		return NullNetwork
	default:
		panic(fmt.Sprintf("unknown network type: %v", s))
	}
}

// MarshalJSON ensures that json conversions use the string value here, not the int value
func (n *NetworkType) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("\"%v\"", n.String())), nil
}

// ValidateWithSubnet ensures that a subnet is valid for a given NetworkType
func (n NetworkType) ValidateWithSubnet(s string) error {
	if (n == 3) && (s == "") {
		return errors.New("must provide a subnet when using CustomNetwork")
	}
	if (n != 3) && (s != "") {
		return errors.New("must NOT provide a subnet when not using CustomNetwork")
	}
	return nil
}

// Network is the object that represents a Clockworks Network
type Network struct {
	ID     int    `json:"id"`
	Subnet string `json:"subnet,omitempty"`
	Name   string `json:"name,omitempty"`
	Type   string `json:"type,omitempty"`
	VRF    string `json:"vrf,omitempty"`
}

// NetworkService holds on the methods for interacting with the Network endpoint
type NetworkService interface {
	List(context.Context) ([]Network, *Response, error)
	ListCIDRs(context.Context) ([]string, *Response, error)
	ListWithLocation(context.Context, int) ([]Network, *Response, error)
	Get(context.Context, string) (*Network, *Response, error)
	ShellComplete(context.Context, string) []string
}

// NetworkServiceOp is the operator for the NetworkService
type NetworkServiceOp struct {
	client *Client
}

// ListCIDRs returns a list of CIDRS
func (s *NetworkServiceOp) ListCIDRs(ctx context.Context) ([]string, *Response, error) {
	ret := []string{}
	n, resp, err := s.List(ctx)
	if err != nil {
		return nil, resp, err
	}
	for _, i := range n {
		ret = append(ret, i.Subnet)
	}
	return ret, resp, nil
}

// List returns a slice of all Network objects
func (s *NetworkServiceOp) List(ctx context.Context) ([]Network, *Response, error) {
	path := "api/v1/networks"

	req := s.client.mustNewRequest(http.MethodGet, path, nil)

	// root := new(accountRoot)
	var items []Network
	resp, err := s.client.Do(ctx, req, &items)
	if err != nil {
		return nil, resp, err
	}

	return items, resp, err
}

// Get returns a single Network object given a name
func (s *NetworkServiceOp) Get(ctx context.Context, name string) (*Network, *Response, error) {
	path := fmt.Sprintf("api/v1/networks/%v", name)

	req := s.client.mustNewRequest(http.MethodGet, path, nil)

	// root := new(accountRoot)
	var item Network
	resp, err := s.client.Do(ctx, req, &item)
	if err != nil {
		return nil, resp, err
	}

	return &item, resp, err
}

// ListWithLocation returns a list of networks given a location ID
func (s *NetworkServiceOp) ListWithLocation(ctx context.Context, id int) ([]Network, *Response, error) {
	path := fmt.Sprintf("api/v1/locations/%v/networks", id)

	req := s.client.mustNewRequest(http.MethodGet, path, nil)

	// root := new(accountRoot)
	var target []Network
	resp, err := s.client.Do(ctx, req, &target)
	if err != nil {
		return nil, resp, err
	}

	return target, resp, err
}

// ShellComplete provides shell completion for VM stuff
func (s *NetworkServiceOp) ShellComplete(ctx context.Context, filter string) []string {
	items, _, err := s.List(ctx)
	if err != nil {
		panic(err)
	}
	var ret []string
	for _, i := range items {
		if strings.Contains(i.Name, filter) {
			ret = append(ret, fmt.Sprintf("%v\t%v", i.Subnet, i.Name))
		}
	}
	return ret
}
