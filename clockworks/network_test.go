package clockworks

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestListNetworks(t *testing.T) {
	// Create a new client
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/networks", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)

		content, _ := os.ReadFile("testdata/networks.json")
		fmt.Fprint(w, string(content))
	})

	items, _, err := client.Network.List(ctx)
	require.NoError(t, err)
	require.IsType(t, []Network{}, items)
	require.Greater(t, len(items), 0)
}

func TestGetNetwork(t *testing.T) {
	// Create a new client
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/networks/foo", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)

		content, _ := os.ReadFile("testdata/networks-foo.json")
		fmt.Fprint(w, string(content))
	})

	items, _, err := client.Network.Get(ctx, "foo")
	require.NoError(t, err)
	require.IsType(t, &Network{}, items)
}

func TestListWithLocation(t *testing.T) {
	// Create a new client
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/locations/1/networks", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)
		content, _ := os.ReadFile("testdata/locations/1-networks.json")
		fmt.Fprint(w, string(content))
	})

	items, _, err := client.Network.ListWithLocation(ctx, 1)
	require.NoError(t, err)
	require.NotNil(t, items)
}

func TestValidateNetwork(t *testing.T) {
	setup()
	defer teardown()
	mux.HandleFunc("/api/v1/networks", func(w http.ResponseWriter, _ *http.Request) {
		content, _ := os.ReadFile("testdata/networks.json")
		fmt.Fprint(w, string(content))
	})
	ts := []struct {
		n       NetworkType
		s       string
		wantErr bool
	}{
		// Valid Private network
		{n: PrivateNetwork, s: "", wantErr: false},
		// Valid Public network
		{n: PrivateNetwork, s: "", wantErr: false},
		// Missing custom subnet
		{n: CustomNetwork, s: "", wantErr: true},
		// Valid custom subnet
		{n: CustomNetwork, s: "10.236.192.96/28", wantErr: false},
		// Subnet specified with private network type
		{n: PrivateNetwork, s: "10.236.192.96/28", wantErr: true},
	}

	for _, tt := range ts {
		if tt.wantErr {
			require.Error(t, tt.n.ValidateWithSubnet(tt.s))
		} else {
			require.NoError(t, tt.n.ValidateWithSubnet(tt.s))
		}
	}
}

func TestNetworkTypeWithString(t *testing.T) {
	tests := map[string]struct {
		s         string
		w         NetworkType
		wantPanic string
	}{
		"private": {
			s: "private",
			w: PrivateNetwork,
		},
		"public": {
			s: "public",
			w: PublicNetwork,
		},
		"custom": {
			s: "custom",
			w: CustomNetwork,
		},
		"unknown-network": {
			s:         "Never-exists",
			wantPanic: "unknown network type: Never-exists",
		},
	}

	for desc, tt := range tests {
		tt := tt
		if tt.wantPanic != "" {
			require.PanicsWithValue(t, tt.wantPanic, func() { NetworkTypeWithString(tt.s) }, desc)
		} else {
			require.Equal(t, tt.w, NetworkTypeWithString(tt.s), desc)
		}
	}
}

func TestNetworkShellComplete(t *testing.T) {
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/networks", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)
		fileWriter(w, "testdata/networks.json")
	})

	got := client.Network.ShellComplete(context.TODO(), "")
	require.NotNil(t, got)
	require.Greater(t, len(got), 0)
}

func TestListCIDRs(t *testing.T) {
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/networks", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)
		fileWriter(w, "testdata/networks.json")
	})

	items, _, err := client.Network.ListCIDRs(context.TODO())
	require.NoError(t, err)
	require.Greater(t, len(items), 0)
}
