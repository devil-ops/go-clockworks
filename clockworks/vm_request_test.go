package clockworks

import (
	"fmt"
	"net/http"
	"os"
	"sync/atomic"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestListVMRequests(t *testing.T) {
	// Create a new client
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/vm_requests", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)

		content, _ := os.ReadFile("testdata/vm_requests.json")
		fmt.Fprint(w, string(content))
	})

	items, _, err := client.VMRequest.List(ctx, nil)
	require.NoError(t, err)
	require.IsType(t, []VMRequest{}, items)
	require.Greater(t, len(items), 0)
}

func TestGetVMRequest(t *testing.T) {
	// Create a new client
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/vm_requests/1", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)

		content, _ := os.ReadFile("testdata/vm_request.json")
		fmt.Fprint(w, string(content))
	})

	items, _, err := client.VMRequest.Get(ctx, 1)
	require.NoError(t, err)
	require.IsType(t, &VMRequest{}, items)
}

func TestListVMRequestsWithVMId(t *testing.T) {
	// Create a new client
	setup()
	defer teardown()

	mux.HandleFunc("/api/v1/vms/1/vm_requests", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)

		content, _ := os.ReadFile("testdata/vm_requests.json")
		fmt.Fprint(w, string(content))
	})

	items, _, err := client.VMRequest.ListWithVMId(ctx, 1, nil)
	require.NoError(t, err)
	require.IsType(t, []VMRequest{}, items)
	require.Greater(t, len(items), 0)
}

func TestWaitForVMRequestStatus(t *testing.T) {
	setup()
	defer teardown()

	// Use a counter to replicate changing statuses
	count := uint32(0)
	statuses := []string{"pending", "running", "completed"}

	mux.HandleFunc("/api/v1/vm_requests/1", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, r.Method, http.MethodGet)

		st := statuses[atomic.LoadUint32(&count)%uint32(len(statuses))]
		fmt.Fprintf(os.Stderr, "STATUS: %+v\n", st)
		content := fmt.Sprintf(`{"id":1,"status":"%s"}`, st)

		atomic.AddUint32(&count, 1)

		// Unsure why i can't get require.Greater to work here
		if count > 5 {
			t.Fatal("Expected to wait for VMRequest status to change, but never did")
		}
		fmt.Fprint(w, content)
	})

	err := client.VMRequest.WaitForFinalStatus(ctx, 1)
	require.NoError(t, err)
}
