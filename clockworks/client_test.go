package clockworks

import (
	"context"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/require"
)

var (
	mux    *http.ServeMux
	ctx    = context.TODO()
	client *Client
	server *httptest.Server
)

func setup() {
	mux = http.NewServeMux()
	server = httptest.NewServer(mux)

	/*
		client = NewClient(nil, "foo", "bar", "baz")
		url, _ := url.Parse(server.URL)
		client.BaseURL = url
	*/

	// New way to do the client
	client = New(
		WithReqWaitTimes(map[string]time.Duration{}),
		WithUser("some-person"),
		WithToken("some-token"),
		WithURL(server.URL),
	)
}

func teardown() {
	server.Close()
}

func TestNew(t *testing.T) {
	c := New(
		WithUser("foo"),
		WithToken("bar"),
		WithURL("https://example.com"),
	)
	require.NotNil(t, c)
	require.Equal(t, "foo", c.Username)
	require.Equal(t, "bar", c.Token)
}

func TestNewBadURL(t *testing.T) {
	require.Panics(t, func() { New(WithURL("%+o")) })
}

func TestWithViper(t *testing.T) {
	v := viper.New()
	v.Set("username", "my-username")
	v.Set("token", "my-token")
	c := New(WithViper(v))
	require.NotNil(t, c)
}

func TestWithEnv(t *testing.T) {
	tests := map[string]struct {
		env         map[string]string
		expectPanic string
	}{
		"good": {
			env: map[string]string{
				"CLOCKWORKS_USERNAME": "some-user",
				"CLOCKWORKS_TOKEN":    "some-token",
				"CLOCKWORKS_URL":      "https://example.com",
			},
			expectPanic: "",
		},
		"good-no-url": {
			env: map[string]string{
				"CLOCKWORKS_USERNAME": "some-user",
				"CLOCKWORKS_TOKEN":    "some-token",
			},
			expectPanic: "",
		},
		"missing-username": {
			env: map[string]string{
				"CLOCKWORKS_TOKEN": "some-token",
			},
			expectPanic: "CLOCKWORKS_USERNAME is required",
		},
		"missing-token": {
			env: map[string]string{
				"CLOCKWORKS_USERNAME": "some-user",
			},
			expectPanic: "CLOCKWORKS_TOKEN is required",
		},
	}
	for desc, tt := range tests {
		os.Clearenv()
		for ek, ev := range tt.env {
			t.Setenv(ek, ev)
		}
		if tt.expectPanic == "" {
			require.NotNil(t, New(WithEnv()), desc)
		} else {
			require.PanicsWithValue(t, tt.expectPanic, func() { New(WithEnv()) }, desc)
		}

		// Ok, now try it using the backwards compatible NewWithEnvironment function
		if tt.expectPanic == "" {
			c, err := NewFromEnvironment()
			require.NoError(t, err, desc)
			require.NotNil(t, c, desc)
		} else {
			c, err := NewFromEnvironment()
			require.Error(t, err, desc)
			require.Nil(t, c, desc)
		}
	}
}

func TestErrWriter(t *testing.T) {
	c := New(withFakeCredentials(), WithErrWriter(os.Stdout))
	require.NotNil(t, c)
}

func TestClientValidate(t *testing.T) {
	tests := map[string]struct {
		client  *Client
		wantErr string
	}{
		"good": {
			client: &Client{
				Token:    "foo",
				Username: "bar",
			},
		},
		"missing-user": {
			client: &Client{
				Token: "bar",
			},
			wantErr: "must set a user",
		},
		"missing-token": {
			client: &Client{
				Username: "bar",
			},
			wantErr: "must set a token",
		},
	}
	for desc, tt := range tests {
		err := tt.client.Validate()
		if tt.wantErr == "" {
			require.NoError(t, err, desc)
		} else {
			require.EqualError(t, err, tt.wantErr)
		}
	}
}
