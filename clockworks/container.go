package clockworks

import (
	"context"
	"errors"
	"fmt"
	"net/http"
)

// Container is the clockworks 'container'
type Container struct {
	Name string `json:"name,omitempty"`
}

// NewContainer returns a new Container instance with the provided name
func NewContainer(n string) *Container {
	return &Container{
		Name: n,
	}
}

// ValidateWithSysadminOption returns an error if it's invalid
func (c Container) ValidateWithSysadminOption(s SysadminOption) error {
	if c.Name == "" && (s == 1) {
		return errors.New("container required when using self-admin")
	}
	return nil
}

// ContainerService holds on the methods for interacting with containers
type ContainerService interface {
	List(context.Context) ([]Container, *Response, error)
	ListNames(context.Context) ([]string, *Response, error)
	Get(context.Context, string) (*Container, *Response, error)
}

// ContainerServiceOp is the operator for the ContainerService interface
type ContainerServiceOp struct {
	client *Client
}

// ListNames returns a list of the containers as strings
func (s *ContainerServiceOp) ListNames(ctx context.Context) ([]string, *Response, error) {
	ret := []string{}
	n, resp, err := s.List(ctx)
	if err != nil {
		return nil, resp, err
	}
	for _, i := range n {
		ret = append(ret, i.Name)
	}
	return ret, resp, nil
}

// List returns the container objects
func (s *ContainerServiceOp) List(ctx context.Context) ([]Container, *Response, error) {
	path := "api/v1/containers"

	req := s.client.mustNewRequest(http.MethodGet, path, nil)

	// root := new(accountRoot)
	var items []Container
	resp, err := s.client.Do(ctx, req, &items)
	if err != nil {
		return nil, resp, err
	}

	return items, resp, err
}

// Get returns a specific container object using it's name string
func (s *ContainerServiceOp) Get(ctx context.Context, name string) (*Container, *Response, error) {
	path := fmt.Sprintf("api/v1/containers/%v", name)

	req := s.client.mustNewRequest(http.MethodGet, path, nil)

	// root := new(accountRoot)
	var item Container
	resp, err := s.client.Do(ctx, req, &item)
	if err != nil {
		return nil, resp, err
	}

	return &item, resp, err
}
