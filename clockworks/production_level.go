package clockworks

import (
	"fmt"

	"github.com/spf13/cobra"
)

// ProductionLevel represents the production level status of a VM
type ProductionLevel int

var productionLevelMap = map[string]ProductionLevel{
	"production":  Production,
	"development": Development,
	"testing":     Testing,
	"":            NullProductionLevel,
}

// ProductionLevelCompletion returns shell completion for the sysadmin options
func ProductionLevelCompletion(_ *cobra.Command, _ []string, _ string) ([]string, cobra.ShellCompDirective) {
	return nonEmptyKeys(productionLevelMap), cobra.ShellCompDirectiveDefault
}

const (
	// NullProductionLevel is the unset value for this type
	NullProductionLevel ProductionLevel = iota
	// Production is...prime time production my folks!
	Production
	// Testing is for testing changes before pushing to production
	Testing
	// Development is for breaking changes
	Development
)

func (p ProductionLevel) String() string {
	m := reverseMap(productionLevelMap)
	if v, ok := m[p]; ok {
		return v
	}
	panic("invalid production_level")
}

// Type satisfies part of the pflags.Value interface
func (p ProductionLevel) Type() string {
	return "ProductionLevel"
}

// Set helps fulfill the pflag.Value interface
func (p *ProductionLevel) Set(v string) error {
	if v, ok := productionLevelMap[v]; ok {
		*p = v
		return nil
	}
	return fmt.Errorf("ProductionLevel should be one of: %v", nonEmptyKeys(productionLevelMap))
}

// MarshalJSON ensures that json conversions use the string value here, not the int value
func (p *ProductionLevel) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("\"%v\"", p.String())), nil
}
