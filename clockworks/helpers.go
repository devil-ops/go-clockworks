package clockworks

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"reflect"
	"sort"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func containsString(slice []string, s string) bool {
	for _, item := range slice {
		if item == s {
			return true
		}
	}
	return false
}

func containsInt(slice []int, s int) bool {
	for _, item := range slice {
		if item == s {
			return true
		}
	}
	return false
}

func mustParseURL(u string) *url.URL {
	ur, err := url.Parse(u)
	if err != nil {
		panic(err)
	}
	return ur
}

func mustNewRequest(m, u string, b io.Reader) *http.Request {
	r, err := http.NewRequest(m, u, b)
	if err != nil {
		panic(err)
	}
	return r
}

// envOrPanic gets an environment variable test OR panics with the given error
func envOrPanic(e, p string) string {
	got := os.Getenv(e)
	if got == "" {
		panic(p)
	}
	return got
}

// viperOrPanic gets a viper variable OR panics with the given error
func viperOrPanic[T string](e, p string, v *viper.Viper) T {
	switch any(new(T)).(type) {
	case *string:
		got := v.GetString(e)
		if got == "" {
			panic(p)
		}
		return any(got).(T)
	default:
		panic("unexpected use of viperOrPanic")
	}
}

// envWithDefault returns an environment variable value, or (if empty), a default string
func envWithDefault(e, d string) string {
	got := os.Getenv(e)
	if got == "" {
		return d
	}
	return got
}

// reverseMap takes a map[k]v and returns a map[v]k
func reverseMap[K string, V string | SysadminOption | StorageType | ProductionLevel](m map[K]V) map[V]K {
	ret := make(map[V]K, len(m))
	for k, v := range m {
		ret[v] = k
	}
	return ret
}

// nonEmptyKeys returns the non-empty keys of a map in an array
func nonEmptyKeys[V any](m map[string]V) []string {
	var ret []string
	for k := range m {
		if k != "" {
			ret = append(ret, k)
		}
	}
	sort.Strings(ret)
	return ret
}

// mustGetCmd uses generics to get a given flag with the appropriate Type from a cobra.Command
func mustGetCmd[T []int | int | string | bool | time.Duration | StorageType | ProductionLevel](cmd *cobra.Command, s string) T {
	switch any(new(T)).(type) {
	case *int:
		item, err := cmd.Flags().GetInt(s)
		panicIfErr(err)
		return any(item).(T)
	case *string:
		item, err := cmd.Flags().GetString(s)
		panicIfErr(err)
		return any(item).(T)
	case *bool:
		item, err := cmd.Flags().GetBool(s)
		panicIfErr(err)
		return any(item).(T)
	case *[]int:
		item, err := cmd.Flags().GetIntSlice(s)
		panicIfErr(err)
		return any(item).(T)
	case *StorageType:
		item, err := cmd.Flags().GetString(s)
		panicIfErr(err)
		return any(item).(T)
	case *time.Time:
		item, err := cmd.Flags().GetDuration(s)
		panicIfErr(err)
		return any(item).(T)
	default:
		panic(fmt.Sprintf("unexpected use of mustGetCmd: %v", reflect.TypeOf(s)))
	}
}

func panicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}

func mustRegisterCompletion(client *Client, cmd *cobra.Command, name string, f func(context.Context, string) []string) {
	err := registerCompletion(client, cmd, name, f)
	if err != nil {
		panic(err)
	}
}

// registerCompletion is just a little wrapper function to register a an autocompletion
func registerCompletion(client *Client, cmd *cobra.Command, name string, f func(context.Context, string) []string) error {
	if client != nil {
		if err := cmd.RegisterFlagCompletionFunc(name, func(_ *cobra.Command, _ []string, toComplete string) ([]string, cobra.ShellCompDirective) {
			return f(context.TODO(), toComplete), cobra.ShellCompDirectiveDefault
		}); err != nil {
			return err
		}
	} else {
		if err := cmd.RegisterFlagCompletionFunc(name, func(_ *cobra.Command, _ []string, _ string) ([]string, cobra.ShellCompDirective) {
			return nil, cobra.ShellCompDirectiveNoFileComp
		}); err != nil {
			return err
		}
	}
	return nil
}
